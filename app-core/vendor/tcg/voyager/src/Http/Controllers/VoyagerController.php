<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VoyagerController extends Controller
{
    public function index()
    {
        return Voyager::view('voyager::index');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function systemBackup(){
        $tables = DB::select("SHOW TABLES LIKE 'posts_%'");

        return view('frontend.backup', compact('tables'));
    }
    
    public function backup_posts(){
         for($i=1;$i<=10; $i++){
            if (Schema::hasTable("posts_".date("Y")."_10")){
                return '0';
                break;
            }else{
                $saved_posts = Post::query()->where('published_at','<', now()->subMonths(3))->where('category_id', '!=', 8)->where('category_id', '!=', 9)->where('category_id', '!=', 10)->where('category_id', '!=', 11)->get()->toArray();

                $table_name = "posts_".date("Y")."_$i";
                if (!Schema::hasTable($table_name) && !empty($saved_posts)){
                        Schema::create($table_name, function (Blueprint $table) {
                        $table->increments('id');
                        $table->integer('author_id');
                        $table->integer('category_id')->nullable();
                        $table->longText('title');
                        $table->longText('seo_title')->nullable();
                        $table->text('excerpt')->nullable();
                        $table->text('body');
                        $table->string('image' , 255)->nullable();
                        $table->string('slug' , 255);
                        $table->text('meta_description')->nullable();
                        $table->text('meta_keywords')->nullable();
                        $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING']);
                        $table->tinyInteger('featured')->nullable();
                        $table->nullableTimestamps();
                        $table->timestamp('published_at')->nullable();
                        $table->date('date_evenement')->nullable();
                        $table->text('source_link')->nullable();
                        $table->string('source_text', 255)->nullable();
                        $table->integer('planned')->nullable();
                        $table->integer('views_count')->nullable();
                        $table->integer('order_post')->nullable();
                        $table->integer('id_campaign')->nullable();
                    });
                    
                    foreach (array_chunk($saved_posts,1000) as $t) {
                    
                       DB::table($table_name)->insert($t);
                    }
                    $saved_posts = Post::query()->where('published_at','<', now()->subMonths(3))->where('category_id', '!=', 8)->where('category_id', '!=', 9)->where('category_id', '!=', 10)->where('category_id', '!=', 11)->delete();
                    
                    return '1';
                    break;
                    
                      
                }
                
            }
            
            
        }
        return '2';
    }


    public function recover_posts(Request $request){

            $table_name_to_recover_from = $request->table_name;

            $saved_posts = DB::table($table_name_to_recover_from)->get();

            $arrayData = array_map(function($item) {
                return (array)$item; 
            }, $saved_posts->toArray());


                    
            foreach (array_chunk($arrayData,1000) as $t) {
                    
                DB::table('posts')->insert($t);
            }

            Schema::dropIfExists($table_name_to_recover_from);
                      
            return '1';     
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug.'/'.date('F').date('Y').'/';

        $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        }

        $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('".Voyager::image($fullFilename)."'); </script>";
    }

    public function assets(Request $request)
    {
        try {
            $path = dirname(__DIR__, 3).'/publishable/assets/'.Util::normalizeRelativePath(urldecode($request->path));
        } catch (\LogicException $e) {
            abort(404);
        }

        if (File::exists($path)) {
            $mime = '';
            if (Str::endsWith($path, '.js')) {
                $mime = 'text/javascript';
            } elseif (Str::endsWith($path, '.css')) {
                $mime = 'text/css';
            } else {
                $mime = File::mimeType($path);
            }
            $response = response(File::get($path), 200, ['Content-Type' => $mime]);
            $response->setSharedMaxAge(31536000);
            $response->setMaxAge(31536000);
            $response->setExpires(new \DateTime('+1 year'));

            return $response;
        }

        return response('', 404);
    }
}
