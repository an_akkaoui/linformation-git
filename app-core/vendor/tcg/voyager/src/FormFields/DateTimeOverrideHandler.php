<?php

namespace TCG\Voyager\FormFields;
use TCG\Voyager\FormFields\AbstractHandler;
class DateTimeOverrideHandler extends AbstractHandler
{
    protected $codename = 'datetime_override';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.datetime_override', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
