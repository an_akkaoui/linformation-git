<?php

namespace TCG\Voyager\FormFields;

class RichTextBoxOverrideHandler extends AbstractHandler
{
    protected $codename = 'rich_text_box_override';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.rich_text_box_override', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
