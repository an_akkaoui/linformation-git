<?php

namespace TCG\Voyager\FormFields;
use TCG\Voyager\FormFields\AbstractHandler;
class TextOverrideHandler extends AbstractHandler
{
    protected $codename = 'text_overrided';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.text_overrided', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
