<?php

namespace TCG\Voyager\FormFields;
use TCG\Voyager\FormFields\AbstractHandler;
class SelectDropdownOverrideHandler extends AbstractHandler
{
    protected $codename = 'select_dropdown_over';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager::formfields.select_dropdown_override', [
            'row'             => $row,
            'options'         => $options,
            'dataType'        => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
