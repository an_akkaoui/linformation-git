  <input type="text"
       class="form-control"
       name="{{ $row->field }}"
       @if($row->required == 1) required @endif
       @if(isset($options->disabled)) readonly @endif
       placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{old($row->field)}}@endif">