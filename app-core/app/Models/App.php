<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Storage;
use \Torann\GeoIP\GeoIP;

class App extends Model
{
    public function inscrption_a_la_newsletter($request)
    {
        // $f_name = explode(' ', $request["nom_complet"]);
        // $nom = trim($f_name[0]);
        // $prenom = !empty($f_name[1]) ? $f_name[1] : '';

      return  DB::table('inscrits')->insert(
    [
      'civilite' => $request["civilite"],
      'nom' => $request["nom"],
      'prenom' => $request["prenom"],
      'email' => $request["email"],
      'fonction' => $request["fonction"],
      'DepartementCadre' => $request["DepartementCadre"],
      'departementProfessionL' => $request["departementProfessionL"],
      'departementassistanteD' => $request["departementassistanteD"],
      'departementAutres' => $request["departementAutres"],
      'date_naissance' => $request["date"],
      'telephone' => $request["telephone"],
      'entreprise' => $request["entreprise"],
      'ville' => $request["ville"],
      'acceptance1' => $request["acceptance1"],
      'acceptance2' => $request["acceptance2"],
      'acceptance3' => $request["acceptance3"],
    ]
      );
    }
    public function topPosts()
    {
        return DB::table('posts')
        ->select( 'terms.slug as categorie', 'posts.*', 'postmeta.*')
        ->Join('postmeta', 'posts.ID', '=','postmeta.post_id')
        ->leftJoin('term_relationships', 'posts.ID', '=', 'term_relationships.object_id')
        ->leftJoin('term_taxonomy', function ($join) {
            $join->on('term_relationships.term_taxonomy_id', '=', 'term_taxonomy.term_taxonomy_id')
                 ->where('taxonomy', '=', 'category');
           })
        ->Join('terms',  'terms.term_id','=','term_taxonomy.term_id')
        ->where('post_type', '=', 'post')
        ->where(function($query){
            $query->where('post_status', '=', 'publish')
                      ->orWhere(function($query2){
                $query2->where('post_status', '=', 'future')
                          ->where('post_date', '<=', 'NOw()');
                 });
         })
        ->where('meta_key', '=', 'post_views_count')
          ->orderByDesc(DB::raw("meta_value+0"), 'DESC')
          ->limit(6)
          ->get();

    }
    public function getBanniers($url="")
    {
        $bans= DB::table('banniers')
        ->select( 'id_zone', 'id_bannier', 'url_image')
        ->get();
        $ads["ads"]=array();
        $ads["infos"]=array();
          foreach ($bans as $ban) {
            if($ban->id_bannier){
            $src= $url.'/admin?_dnlink='.$ban->id_bannier.'&aid='.$ban->id_zone.'&t='.time();
            $ads["ads"][$ban->id_zone]='<a class="img-ads" href="'.$src.'" target="_blank">'.$ban->url_image.'</a>';
            $ads["infos"][$ban->id_zone]["id_zone"]=$ban->id_zone;
            $ads["infos"][$ban->id_zone]["id_bannier"]=$ban->id_bannier;

          }else{
             $ads["ads"][$ban->id_zone]="";
             $ads["infos"][$ban->id_zone]["id_zone"]="";
            $ads["infos"][$ban->id_zone]["id_bannier"]="";
          }
        }

        return $ads;
    }


    public function getPopup($url="")
    {
        $bans = DB::select( DB::raw(" SELECT * FROM wp_posts WHERE post_type = 'adni_adzones' AND post_title LIKE '%test ban%' LIMIT 1 ") );
        if(count($bans) > 0){
            $id_bannier = $bans[0]->ID;
            $metas = DB::select( DB::raw(" SELECT * FROM `wp_postmeta` WHERE `post_id` = $id_bannier ") );
            $bansmetas = DB::select( DB::raw(" SELECT * FROM `wp_banniers` WHERE `id_zone` =  $id_bannier ") );
            $key = $metas[0]->meta_value;
            return $metas;
            //$idZone = $bansmetas[0]->id_zone;
            /*if(isset($key)){
                $getStatu = unserialize($key);
                if($getStatu['status'] == 'active'){
                    $src= $url.'/admin?_dnlink='.$id_bannier.'&aid='.$idZone.'&t='.time();
                    return $src;
                }

                else
                    return 'noactive';
            }
            else
                return 'NO';*/
        }
        else
            return 'vide';
    }

    public function getCategories($findBeyKey="")
    {
      $categories= array(
        0 => ['key'=> 'actualite','label' =>'Actualité','class' =>'orange_hover','pub_id' =>'7803'],
        1 => ['key'=> 'politique', 'label' =>'Politique','class' =>'gray_hover','pub_id' =>'7809'],
        2 => ['key'=> 'finance-economie', 'label' =>'Finance & Economie','class' =>'redo_hover','pub_id' =>'8114'],
        3 => ['key'=> 'sport', 'label' =>'Sport','class' =>'green_hover','pub_id' =>'8115'],
        4 => ['key'=> 'culture', 'label' =>'Culture','class' =>'cremy_hover','pub_id' =>'8118'],
        5 => ['key'=> 'monde', 'label' =>'Monde','class' =>'blue_hover','pub_id' =>'8119'],
        6 => ['key'=> 'high-tech', 'label' =>'High Tech','class' =>'banaf_hover','pub_id' =>'8120'],
        7 => ['key'=> 'lifestyle', 'label' =>'Lifestyle','class' =>'rose_hover','pub_id' =>null],
        8 => ['key'=> 'videos', 'label' =>'Vidéos','class' =>'red_hover','pub_id' =>null],
        9 => ['key'=> 'people', 'label' =>'People','class' =>'blue_ciel_hover','pub_id' =>null],
      );

      if($findBeyKey){
      $found =false;
        foreach ($categories as $categorie) {
          if($findBeyKey==$categorie["key"]){
            $found =true;
            return $categorie;
          }
        }
      }

      if(isset($found)){
        return null;
      }
      return $categories;
    }

    public function insertLogToDB()
    {

      $lines = file('../app/storage/app/file.txt');
      $insertion=array();
        foreach ($lines as $line) {
            $arrayval=explode(";", $line);
            if(!empty($arrayval))
            $insertion[] = array(
              'event_type' =>  strval($arrayval[0]),
              'id_1' =>  $arrayval[1],
              'id_2' =>  $arrayval[2],
              'id_3' =>  $arrayval[3],
              'notes' =>  strval($arrayval[4]),
              'id' =>  $arrayval[5],
              'tm' =>  $arrayval[6]
                                );
        }
        if(!empty($insertion)){
          DB::table('strack_ev2')->insert($insertion);
        }

    }
    private function getIp(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
        }
    }

     private function getPlatform($agent)
 {
     if ($agent->isPhone()) {
         return 'phone';
     }
     if ($agent->isTablet()) {
         return 'tablet';
     }
     if ($agent->isMobile()) {
         return 'mobile';
     }
     if ($agent->isDesktop()) {
         return 'desktop';
     }
     if ($agent->isRobot()) {
         return 'robot';
     }
     return null;
 }
    public function getinfosagent($user_age=null)
    {
          $infos=array();
          $agent = new Agent();
            if(!$user_age){
              $req=Request();
             $infos['user_agent']=$req->header('User-Agent');
            }else{
             $infos['user_agent']=$user_age;
            }

           $infos['browser']=$agent->browser();
           $lang=$agent->languages();
           if(isset($lang[0])){
           $infos['language']=$lang[0];
           }else{
           $infos['language']="";
           }
           $infos['platform'] =$agent->platform();
           $infos['device'] =$this->getPlatform($agent);//desktop
           $infos['ip']=$this->getIp();
           $locaton=geoip()->getLocation($infos['ip']);
           $infos['country']=$locaton["iso_code"];
           $infos['tm']=time();
           $infos['content_type']="post";
           return $infos;
    }

/*    public function inserttoLogFile($banniers=null,$typepage=0,$id_ban=0,$user_age=null)
    {


    }*/
    public function inserttoLogFile($banniers=null,$typepage=0,$id_ban=0,$user_age=null)
    {

          $user_agent= $this->getinfosagent();

        if(isset($_COOKIE['infostrack'])){
           $cookie=$_COOKIE['infostrack']; // wp_strack_st_id
           $infostrack=explode('+//+', $cookie); // wp_strack_st_id


          }else{
             $track_id = DB::table('options')
                    ->where('option_name', '=', 'strack_track_id')
                    ->value('option_value');
            $track_id++;
            DB::table('options')
            ->where('option_name', 'strack_track_id')
            ->update(['option_value' => $track_id]);

            $id=DB::table('strack_st')->insertGetId(
                ['track_id' => $track_id, 'ip' => $user_agent['ip'],'country'=>$user_agent['country'],'browser'=>$user_agent['browser'],'platform'=>$user_agent['platform'],'device'=>$user_agent['device'],'language'=>$user_agent['language'],'user_agent'=>$user_agent['user_agent'],'content_type'=>$user_agent['content_type'],'tm'=>$user_agent['tm']]
            );


            $infostrack[0]=$track_id;
            $infostrack[1]=$id;

            $cookie=$track_id.'+//+'.$id;
            setcookie("infostrack", $cookie,time()+603600);
            //session()->put('infostrack', $infostrack);
          }

    if(isset($infostrack[1])){
          /*$numItems = count($banniers);
          $i = 0;*/

          $insertdata=array();
          $firsttime=false;

        if(!isset($_COOKIE['showOnlyOne'])){
          $firsttime=true;
        }

          $banniers_actif= $this->getbanniersperpage($typepage,$id_ban,$firsttime);
          foreach ($banniers as $ban) {
            /* if(++$i === $numItems) {
               $n="";
             }else{
               $n="\n";
             }
             $array="'impression'; 0; 0; 517; 'adning::id_1::banner_id,id_2::adzone_id,id_3::advertiser_id'; 21; 1555581052$n";
              */
            if(in_array($ban["id_zone"], $banniers_actif)){
              if($ban["id_zone"] && $ban["id_bannier"])
               array_push($insertdata,array('event_type' => "impression",'id_1' => $ban["id_bannier"],'id_2' => $ban["id_zone"],'id_3' => 517,'id' => $infostrack[1],'tm' =>time(),'notes'=>"adning::id_1::banner_id,id_2::adzone_id,id_3::advertiser_id"));
            }

          }

      if($insertdata)
          DB::table('strack_ev')->insert($insertdata);

        //   Storage::prepend('file.txt',  $array);
        }
        /*else{
          return false;
        }*/


       // Storage::put('file.txt', '');

    }

    public function getbanniersperpage($typepage=0,$id_ban=0,$firsttime=false)
    {
      if($typepage==0){
        if($firsttime)
        return array(11071,11072,11073,7803,8870,7809,8114,8115,8118,8119,8120,12541);
        else
        return array(11071,11072,11073,7803,8870,7809,8114,8115,8118,8119,8120);
      }elseif($typepage==1){
        if($firsttime)
        return array(11071,11072,11073,$id_ban,12541);
        else
        return array(11071,11072,11073,$id_ban);
      }
    }

  


}
