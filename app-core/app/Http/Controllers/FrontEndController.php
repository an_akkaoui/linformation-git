<?php
namespace App\Http\Controllers;
require 'assets/includes/PHPMailer.php';
require 'assets/includes/SMTP.php';
require 'assets/includes/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



use Illuminate\Http\Request;
use App\Models\App;
use App\FrontEnd;
use App\Zone;
use App\Inscrit;
use App\Sondage;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Posts2;
use Hash;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Intervention\Image\Constraint;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
/*use Spatie\Sitemap\SitemapGenerator;*/
//use Illuminate\Support\Facades\Storage;


class FrontEndController extends Controller
{
    function __construct()
    {
        session_start();
        ini_set('max_execution_time', 300);
        setlocale (LC_TIME, 'fr_FR.utf8');
        
        // dd(strftime("%e %b %Y", mktime(0, 0, 0, 12, 22, 1978)));

        // if(isset($_SESSION['dev_acc']) && ($_SESSION['dev_acc'] == 1)){
            
        // }else{
        //     return redirect()->route('acceuil');
        // }
    }

    public function generate_rss_feed($page = 1, $number_of_items = 10){
        $date = new \DateTime();
                                                      
        $posts_feed = Post::all();

        $count = $posts_feed->count();

        $number_of_page = ($number_of_items / $count);

        $page_first_result = ($page-1) * $number_of_items;



        $posts = Post::published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->skip($page_first_result)->take($number_of_items)
                        ->get();


        return response()->view('layouts.feed', compact('posts'))->header('Content-Type', 'application/xml');

    }

    public function crop_imgs($type, $month)
    {
        $images = Post::select('image')->get();
        $frontendmodel = new FrontEnd();
        switch ($type) {
                case '1':
                        $cropimages = $frontendmodel->CropImages($images, 245, 111, 'img_type_1', $month);
                        // code...
                        break;
                case '2':
                        $cropimages = $frontendmodel->CropImages($images, 379, 214, 'img_type_2', $month);
                        // code...
                        break;

                case '3':
                        $cropimages = $frontendmodel->CropImages($images, 231, 154, 'img_type_3', $month);
                        // code...
                        break;
                case '4':
                        $cropimages = $frontendmodel->CropImages($images, 1000, 667, 'img_type_4', $month);
                        // code...
                        break;
                
                default:
                        // code...
                        break;
        }
        
        

    }

    // function dev_log(Request $req){

    //     $pepper = 'f2fbc950b5efd640ea5ea48451e33fd2';
    //     $pwd = $req->psw;
    //     $pwd_peppered = md5($pwd);
    //     // dd($pwd_peppered);
    //     if ($pwd_peppered == $pepper) {
    //         $_SESSION['dev_acc'] = 1;
    //         return redirect()->route('acceuil');
    //     }
    //     else {
    //         return redirect()->route('acceuil');
    //     }
        

    // }

    // function dev_log_out(){
    //     unset($_SESSION['dev_acc']);
    //     return redirect()->route('acceuil');
    // }

    public function index()
    {
        $date = new \DateTime();
        $categories = Category::all();

        $actualite = Post::where('category_id', 1)
                        ->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(5);

        $politique = Post::where('category_id', 2)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5);
        $finance = Post::where('category_id', 3)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5);            
        $sports = Post::where('category_id', 4)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3);       
        $culture = Post::where('category_id', 5)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3); 
        $monde = Post::where('category_id', 6)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(4);      
        $high = Post::where('category_id', 7)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3);                          
        $events = Post::where('category_id', 8)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(1);                  
        $lifestyle = Post::where('category_id', 9)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3);
        $videos = Post::where('category_id', 10)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3); 
        $people = Post::where('category_id', 11)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(3);                                                       
        $posts = Post::where('category_id', 1)
                        ->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->union($lifestyle)
                        ->union($videos)
                        ->union($people)
                        ->where('published_at', '<', $date)
                        ->get();
        

        $zone = new Zone();

        $frontendmodel = new FrontEnd();
        
        $top_posts=$this->topPosts();

        $zones = $zone->getZones();

        $posts = $frontendmodel->SortPosts($posts);

        $events = Post::where('category_id', 8)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();
                        
        $meteo = Post::where('category_id', 12)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();

        $sondage = Sondage::where('status', 1)->orderBy('created_at', 'DESC')->first();

        return view('frontend.index', compact('posts', 'categories', 'top_posts','events', 'meteo', 'sondage'), ['zones'=> $zones]);
        
    }
    

    public function inscription_newsletter(Request $request){
        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'email.email' => 'Cette adresse email est invalide.'
        ];

            $validatedData = $request->validate([
                'email' => 'required|email|max:255|unique:inscrits'
            ],$messages);
            
        if($request->ajax() and $validatedData){
                if(DB::table('inscrits')->insert(['email' => $request->email])){
                    $data["type"]="success";
                    $data["message"]="Completez votre inscription...";
                    return $data;
                }else{
                    $data["type"]="error";
                    $data["message"]="Réessayez plus tard!";
                    return $data;
                }
                
            }else{
                // data is not ajax
                return null;
            }
    }
    
    public function inscription_newsletter_completion(Request $request){
        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'alpha' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            $validatedData = $request->validate([    
                'nom_complet' => 'regex:/^[\pL\s\-]+$/u|required|max:255',               
                'fonction' => 'required|max:255',        
                'DepartementCadre' => 'max:255',        
                'departementProfessionL' => 'max:255',        
                'departementassistanteD' => 'max:255',        
                'departementAutres' => 'max:255',        
                'age' => 'required|max:255',               
                'telephone' => 'nullable|numeric',        
                'ville' => 'required|max:255',
            ],$messages);
        
        $f_name = explode(' ', $request->nom_complet);
        $nom = trim($f_name[0]);
        $prenom = !empty($f_name[1]) ? $f_name[1] : '';
        $DepartementCadre   = !empty($request->DepartementCadre) ? $request->DepartementCadre : '';
        $DepartementProfessionL  = !empty($request->departementProfessionL) ? $request->departementProfessionL : '';
        $DepartementAutres  = !empty($request->departementAutres) ? $request->departementAutres : '';
        $assistanteD  = !empty($request->departementassistanteD) ? $request->departementassistanteD : '';

        if($request->ajax() && $validatedData){
                if(DB::table('inscrits')->where('email',$request->email)->update(['nom' => $nom, 'prenom' => $prenom, 'ville' => $request->ville ,'telephone' => $request->telephone , 'departementCadre' => $DepartementCadre, 'departementProfessionL' => $DepartementProfessionL, 'departementAssistanteD' => $assistanteD, 'departementAutres' => $DepartementAutres, 'fonction' => $request->fonction, 'age' => $request->age])){
                    $data["type"]="success";
                    $data["message"]="Vous avez été inscrit avec succès!";
                    return $data;
                }else{
                    $data["type"]="error";
                    $data["message"]="Réessayez plus tard!";
                    return $data;
                }
                
            }else{
                // data is not ajax
                return null;
            }
    }

    public function newsletter1_old_view()
    {
        $categories = Category::all();

        $politique = Post::where('category_id', 2)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2);
        $finance = Post::where('category_id', 3)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2);            
        $sports = Post::where('category_id', 4)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2);       
        $culture = Post::where('category_id', 5)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2); 
        $monde = Post::where('category_id', 6)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2);      
        $high = Post::where('category_id', 7)->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2);                                                                              
        $posts = Post::where('category_id', 1)
                        ->published()
                        ->orderBy('published_at', 'DESC')
                        ->limit(2)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->where('published_at', '<', new \DateTime())

                        ->get();

        $zone = new Zone();

        $frontendmodel = new FrontEnd();


        $zones = $zone->getZones();

        $posts = $frontendmodel->SortPosts($posts);

        return view('frontend.newsletter1_old', compact('posts', 'categories'), ['zones'=> $zones]);
    }

    public function newsletter1_view()
    {

        $date = new \DateTime();
        $categories = Category::all();

        $politique = Post::where('category_id', 2)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(6);
        $finance = Post::where('category_id', 3)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5);            
        $sports = Post::where('category_id', 4)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(6);       
        $culture = Post::where('category_id', 5)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5); 
        $monde = Post::where('category_id', 6)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(6);      
        $high = Post::where('category_id', 7)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5);                          
        $events = Post::where('category_id', 8)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(5);                  
        $lifestyle = Post::where('category_id', 9)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(4);
        $videos = Post::where('category_id', 10)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(4); 
        $people = Post::where('category_id', 11)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->where('published_at', '<', $date)
                        ->limit(4);                                                       
        $posts = Post::where('category_id', 1)
                        ->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->where('published_at', '<', $date)
                        ->get();

        $zone = new Zone();

        $frontendmodel = new FrontEnd();
        

        $zones = $zone->getNewsletter();
        
        

        $posts = $frontendmodel->SortPosts($posts);

        
        
        return view('frontend.newsletter1', compact('posts', 'categories'), ['zones'=> $zones]);
    }
    
    public function synchro_banniers(){
        $zones = Zone::all();
        $categories = Category::all();

        foreach($categories as $categorie){
            if(isset($categorie->pub_id)){
                $script_ban = $zones[$categorie->pub_id - 1]["web_body_script"];
                
                preg_match('#<script data-cfasync="false" src="(.*?)">(.*?)</script>#is',$script_ban , $banner_link);
                echo $categorie->name."<br>".$zones[$categorie->pub_id - 1]["web_body_script"]."<br>";
                
                if(isset($banner_link[1])){
                    $html = file_get_contents($banner_link[1]);
                    
                    if (preg_match('/"([^"]+)"/', $html, $m)) {
                        
                      
                      if (preg_match('#([0-9]+)#is', urldecode($m[1]), $m2)) {
                          
                          if(isset($m2[1])){
                              
                            $id_ban = $m2[1];
                              
                              
                            $dt = file_get_contents('https://admanager.linformation.ma/get_dates.php?id='.$id_ban);
                           
                            $data = explode("|", $dt);
                           
                            DB::table('zones')->where('id', $categorie->pub_id)->update(['extracted_script_bannier' => urldecode($m[1]), 'debut_bannier' => $data[0], 'fin_bannier' => $data[1], 'newsletter_lien' => $data[2]]);
                            
                          }
                          
                      }
                      
                    }else{
                    DB::table('zones')->where('id', $categorie->pub_id)->update(['extracted_script_bannier' => '', 'debut_bannier' => Carbon::now(), 'fin_bannier' => Carbon::now()]);
                   
                    }
                    
                }else{
                    DB::table('zones')->where('id', $categorie->pub_id)->update(['extracted_script_bannier' => '', 'debut_bannier' => Carbon::now(), 'fin_bannier' => Carbon::now()]);
                    
                   
                }
                
            }
        
        
     
        
        }
        
        dd('Les bannières ont été mises à jour avec succès!');
        
        
    }
    
    public function alert_view()
    {
         

        $date = new \DateTime();

        
        $featured_posts = Post::where('featured',1)->published()->whereDate('published_at', '=',Carbon::today())->get();
        
        if($featured_posts->count() == 0){
            $post = Post::where('featured',1)->published()->whereDate('published_at', Carbon::today())->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')->get();
            
            if($post->count() != 0){
                
                Post::where('id', $post[0]->id)->update(['featured' => 1]);
                
            }else{
                dd('En attente d\'insertion du choix d\'article d\'aujourd\'hui...');
            }
            
            
        }else{
            $post = Post::where('featured',1)->published()->whereDate('published_at', Carbon::today())->get();
        }
        $frontendmodel = new FrontEnd();

        $formatted_time = ucwords(strftime("%A %d %B %Y", $date->getTimestamp()));

        $zone=new Zone();
        
        $banner_alert = $zone->getZones();
            
        
        return view('frontend.alert', compact('post', 'banner_alert'), ['formatted_time' => $formatted_time]);
    }
    
    

    public function contact_view()
    {           
        
        $zone=new Zone();
          
        $zones=$zone->getZones();

        $categories = Category::all();            
                     
        return view("frontend.contact",['categories' =>$categories,'zones' =>$zones]);

    }

    public function inscription_view()
    {           
        $zone=new Zone();
          
        $zones=$zone->getZones();

        $categories = Category::all();           
                      
        return view("frontend.inscription",['categories' =>$categories,'zones' =>$zones]);

    }
    
    public function team_view()
    {           
        $zone=new Zone();
          
        $zones=$zone->getZones();

        $categories = Category::all();           
                      
        return view("frontend.team",['categories' =>$categories,'zones' =>$zones]);

    }

    public function contact_sendmail(Request $request)
    {
        
                $messages = [
                'required' => 'Le champ :attribute est obligatoire.',
                'email.unique' => 'Cette adresse email est déjà utilisée.',
                'regex' => ' Le champ :attribute ne doit comporter que des lettre.',
                'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
                ];

                    $validatedData = $request->validate([
                        'nom' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
                        'prenom' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
                        'subject' => 'required|max:255',        
                        'telephone' => 'required|nullable|numeric',
                        'email' => 'required|max:255',        
                        'message' => 'required|max:100',
                    ],$messages);

                    
                    if($request->ajax() and $validatedData){
                    //  $app=new App();
                        // Mail::to($request["email"])->send(new sendEmail($request["nom"],$request["subject"],$request["telephone"],$request["message"]));
                        $message= "Nom : ".$request["nom"].'<br/>';
                        $message= "Prenom : ".$request["prenom"].'<br/>';
                        $message.= "Email : ".$request["email"].'<br/>';
                        $message.= "Téléphone : ".$request["telephone"].'<br/>';
                        $message.= "Subject : ".$request["subject"].'<br/>';
                        $message.= "message : ".$request["message"].'<br/>';
                        // $headers= "Content-Type: text/html; charset=UTF-8\r\n";
                        // $headers.= 'From: Linformation.ma contactform@linformation.ma' . "\r\n" .
                        // 'X-Mailer: PHP/' . phpversion();
                        $mail = new PHPMailer(true);
                        // SMTP configuration
                        $mail->isSMTP();
                        //$mail->SMTPDebug = 4;
                        $mail->SMTPAuth = true;
                        $mail->SMTPSecure = 'ssl';
                        $mail->Host = 'smtp.gmail.com';
                        $mail->Port = 465;
                        $mail->Username = 'info@wibhelp.com';
                        $mail->Password = 'Wibd@y20@19@';
                        
                        $mail->setFrom('info@wibhelp.com', 'Wibday');
                        $mail->addAddress('info@wibhelp.com');
                        $mail->Subject = 'Linformation.ma - '.$request["subject"];
                        $mail->isHTML(true);
                        $mail->CharSet = 'UTF-8';

                        $mail->Body = $message;
                        
                        if(!$mail->send()){
                            return $mail->ErrorInfo;
                        }else{
                            $data["type"]="success";
                            $data["message"]="Votre Message a été envoyé !";
                            return $data;
                        }
                            
                        
                            
                        
                    }else{
                        // data is not ajax
                        return null;
                    }
                
    }
    public function sondage_vote(Request $request)
    {

        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'regex' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            // $validatedData = $request->validate([
            //     'civilite' => 'alpha|required|max:10',        
            //     'nom' => 'required|max:255',  
            //     'prenom' => 'required|max:255',       
            //     'email' => 'required|max:255|unique:inscrits',        
            //     'fonction' => 'required|max:255',              
            //     'age' => 'required|max:255',                
            //     'telephone' => 'nullable|numeric',        
            //     'ville' => 'required|max:255'
            // ],$messages);


            if($request->ajax()){
                if(isset($request->rep1) && !empty($request->rep1)){
                    $insertion = DB::table('sondages')->increment('rep1_count');
                    $data["type"]="success";
                    $data["message"]="Merci d'avoir voté!";
                    return $data;
                }else if(isset($request->rep2) && !empty($request->rep2)){
                    $insertion = DB::table('sondages')->increment('rep2_count');
                    $data["type"]="success";
                    $data["message"]="Merci d'avoir voté!";
                    return $data;
                }else if(isset($request->rep3) && !empty($request->rep3)){
                    $insertion = DB::table('sondages')->increment('rep3_count');
                    $data["type"]="success";
                    $data["message"]="Merci d'avoir voté!";
                    return $data;
                }else{
                        $data["type"]="danger";
                        $data["message"]="Erreur! Merci de ressayé plus tard!";
                        return $data;
                    }
                    
            }else{
                // data is not ajax
                return null;
            }
         
    } 
    public function inscription_insert(Request $request)
    {

        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'regex' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            $validatedData = $request->validate([
                'civilite' => 'alpha|required|max:10',        
                'nom' => 'required|max:255',  
                'prenom' => 'required|max:255',       
                'email' => 'required|max:255|unique:inscrits',        
                'fonction' => 'required|max:255',              
                'age' => 'required|max:255',                
                'telephone' => 'nullable|numeric',        
                'ville' => 'required|max:255'
            ],$messages);


            if($request->ajax() and $validatedData){
                $app=new App();
                $insertion=$app->inscrption_a_la_newsletter($request);
                if($insertion){
                        $data["type"]="success";
                        $data["message"]="Vous avez été inscrit avec succès!";
                        return $data;
                    }else{
                        $data["type"]="danger";
                        $data["message"]="Erreur! Merci de ressayé plus tard!";
                        return $data;
                    }
                    
            }else{
                // data is not ajax
                return null;
            }
         
    } 

    public function confirmation_inscription(Request $request)
    {           
        
        $categories = Category::all();  

        $zone=new Zone();
          
        $zones=$zone->getZones();

        $confirmation=$request->session()->get('confirmation');       
        if(!$confirmation){
            return redirect()->route('inscription.view');
        }
        return view("frontend.confirmation",['categories' =>$categories,'zones' =>$zones]);

    }
 
    public function categorie($slug)
    {  

        $date = new \DateTime();
        
        $app=new App();

        $top_posts=$this->topPosts();

        $categories = Category::all();  

        $categorie = Category::whereSlug($slug)->with('posts')->firstOrFail();

        if(!$categorie){
            return abort(404);        
        } 
        if($categorie->slug=="events"){
            return abort(404);
        }

        

        $posts = Post::whereCategoryId($categorie->id)->published()->where('published_at', '<', $date)->orderBy('order_post', 'ASC')->orderBy('published_at', 'DESC')->paginate(8);

        $frontendmodel = new FrontEnd();

        $posts_ordered = $frontendmodel->SortPostsCat($posts);

        $zone=new Zone();
          
        $zones=$zone->getZones();
        
        $events = Post::where('category_id', 8)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();
        
        $meteo = Post::where('category_id', 12)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();
        
        $sondage = Sondage::where('status', 1)->orderBy('created_at', 'DESC')->first();


        return view("frontend.categorie", compact('posts', 'posts_ordered', 'events', 'meteo', 'sondage'),['top_posts' =>$top_posts,'categories' =>$categories,'categorie' =>$categorie,'zones' =>$zones]);
    }

    public function topPosts(){

        $posts = Post::orderBy('created_at', 'DESC')->published()->limit(5)->get();

        return $posts;

    }

  
    public function article($categorie="",$postName="",$id=0)
    {
        $date = new \DateTime();
        
        $categories=Category::all();
           
        $cat=$categorie;

        $categorie= Category::whereSlug($categorie)->with('posts')->firstOrFail();          
        
        $related_posts = Post::whereCategoryId($categorie->id)
        ->published()
        ->latest()
        ->where("id","!=",$id)
        ->take(3)
         ->get();  
         
         if ($cat!="events" and (!$id or $id==0 or !$categorie )){
            return abort(404);
        }
        
        if($cat=="events" and (!$id or $id==0)){
            return abort(404);
        }

        $post = Post::find($id);
        
        

        if(!$post){
            return abort(404);        
        }else{
            $post->increment('views_count');
        }
        $zone=new Zone();
          
        $zones=$zone->getZones();

        $top_posts=$this->topPosts();
                    
        $events = Post::where('category_id', 8)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();
        $meteo = Post::where('category_id', 12)->published()
                        ->orderBy('order_post', 'ASC')
                        ->orderBy('published_at', 'DESC')
                        ->limit(1)->get();
                        
        $sondage = Sondage::where('status', 1)->orderBy('created_at', 'DESC')->first();

        return view("frontend.article", compact('events', 'meteo', 'sondage'), ['post' =>$post,'categories' =>$categories,'categorie' =>$categorie,'related_posts' =>$related_posts,'cat'=>$cat,'zones' =>$zones, 'top_posts' =>$top_posts]);

    }

    public function popupzon()
    {
        $pop = '';
        
        $bans = DB::select( DB::raw(" SELECT * FROM wp_posts WHERE post_type = 'adni_banners' AND post_title LIKE '%- pop up' LIMIT 1 ") );
        
        if(count($bans) > 0){
            $id_bannier = $bans[0]->ID;
            $metas = DB::select( DB::raw(" SELECT * FROM `wp_postmeta` WHERE `post_id` = $id_bannier ") );
            $meta_value = '';
            for($d=0; $d<count($metas); $d++){
                if($metas[$d]->meta_key == '_adning_args')
                    $meta_value = $metas[$d]->meta_value;
            }
                
            $key = $metas[0]->meta_value;
            
            if($meta_value != ''){
                $getStatu = unserialize($meta_value);
                // print_r( $getStatu );
                if($getStatu['status'] == 'active'){
                    $adzones = $getStatu['adzones'][0];
                    if(isset($adzones)){
                        $urlsrc = '/admin?_dnlink='.$id_bannier.'&aid='.$adzones.'&t='.time();
                        if($getStatu['banner_content'] != ''){
                            $pop = '<a class="img-ads" href="'.$urlsrc.'" target="_blank">'.$getStatu['banner_content'].'</a>';
                        }
                        else {
                            // echo 'banner_content mkynach';
                            $pop = 'vide';
                        }
                    }
                    else 
                        // echo 'adzones mkynach';
                        $pop = 'vide';
                    
                }
                    
                else 
                    // echo 'noactive';
                    $pop = 'vide';
            }
            else 
                // echo 'NO';
                $pop = 'vide';
        }
        else 
            $pop = 'vide';
        
        return array('pop' => $pop);   
    }
    

    private function getImage($image, $post_date){

        if(!empty($image)){
            $resize_width = 550;
            $resize_height = null;
            $date = Carbon::parse($post_date)->format('FY');
            $path = 'posts/'.$date.'/';
            $filename = basename($image);
            $image = $path.$filename;

        }

        return $image;
        

    }

    function remote_file_exists($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if( $httpCode == 200 ){return true;}
    }

    public function exportCsv(Request $request)
    {
       $fileName = 'inscrits.csv';
       $subscribers = Inscrit::all();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Civilité', 'Nom', 'Prénom', 'Email', 'Téléphone', 'Fonction', 'DepartementCadre', 'DepartementProfessionL', 'DepartementAutres', 'Entreprise', 'Date Naissance', 'Ville', 'Dateinscription','Acceptance1','Acceptance2','Acceptance3');

            $callback = function() use($subscribers, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($subscribers as $subscriber) {
                    $row['Civilite']  = $subscriber->civilite;
                    $row['Nom']    = $subscriber->nom;
                    $row['Prenom']    = $subscriber->prenom;
                    $row['Email']    = $subscriber->email;
                    $row['Telephone']    = $subscriber->telephone;
                    $row['Fonction']    = $subscriber->fonction;
                    $row['DepartementCadre']    = $subscriber->departementCadre;
                    $row['DepartementProfessionL']    = $subscriber->departementProfessionL;
                    $row['DepartementAutres']  = $subscriber->departementAutres;
                    $row['Entreprise']  = $subscriber->entreprise;
                    $row['Date Naissance']  = $subscriber->date_naissance;
                    $row['Ville']  = $subscriber->ville;
                    $row['Dateinscription']  = $subscriber->dateinscription;
                    $row['Acceptance1']  = $subscriber->acceptance1;
                    $row['Acceptance2']  = $subscriber->acceptance2;
                    $row['Acceptance3']  = $subscriber->acceptance3;
                    

                    fputcsv($file, array($row['Civilite'],
                                            $row['Nom'],
                                            $row['Prenom'] ,
                                            $row['Email'], 
                                            $row['Telephone'],
                                            $row['Fonction'],
                                            $row['DepartementCadre'],
                                            $row['DepartementProfessionL'],
                                            $row['DepartementAutres'],
                                            $row['Entreprise'],
                                            $row['Date Naissance'],
                                            $row['Ville'],
                                            $row['Dateinscription'],
                                            $row['Acceptance1'],
                                            $row['Acceptance2'],
                                            $row['Acceptance3']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }  
    
    
}