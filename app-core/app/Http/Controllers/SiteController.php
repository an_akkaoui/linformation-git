<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Corcel\Model\Post ;
use App\Models\App;
use Illuminate\Support\Facades\DB;
/*use Spatie\Sitemap\SitemapGenerator;*/
//use Illuminate\Support\Facades\Storage;

class SiteController extends Controller
{
    function __construct()
    {    
    setlocale (LC_TIME, 'fr_FR');
    }
    
     public function index()
    {        

        $politique = Post::taxonomy('category', 'politique')->newest()->published()
                         ->limit(6);        
        $finance = Post::taxonomy('category', 'finance-economie')->newest()->published()
                         ->limit(5);                 
        $sports = Post::taxonomy('category', 'sport')->newest()->published()
                         ->limit(6);        
        $culture = Post::taxonomy('category', 'culture')->newest()->published()
                         ->limit(5); 
        $monde = Post::taxonomy('category', 'monde')->newest()->published()
                         ->limit(6);     
        $high = Post::taxonomy('category', 'high-tech')->newest()->published()
                         ->limit(5);                         
        $events = Post::taxonomy('category', 'events')->newest()->published()
                         ->limit(5);                     
        $lifestyle = Post::taxonomy('category', 'lifestyle')->newest()->published()
                         ->limit(4);
        $videos = Post::taxonomy('category', 'videos')->newest()->published()
                         ->limit(4);
        $people = Post::taxonomy('category', 'people')->newest()->published()
                         ->limit(4);                                                       
        $posts = Post::taxonomy('category', 'actualite')->newest()->published()
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->union($events)
                        ->union($lifestyle)
                        ->union($videos)
                        ->union($people)
                        ->get();
        $app=new App();
        
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();
        $app->inserttoLogFile($bans["infos"],0);              
        return view("index", ['posts' =>$posts,'categories' =>$categories,'bans' =>$bans["ads"], 'popup' => $this->popup()]);
    }
    public function popupzon()
    {
        $pop = '';
        
        $bans = DB::select( DB::raw(" SELECT * FROM wp_posts WHERE post_type = 'adni_banners' AND post_title LIKE '%- pop up' LIMIT 1 ") );
        
        if(count($bans) > 0){
            $id_bannier = $bans[0]->ID;
            $metas = DB::select( DB::raw(" SELECT * FROM `wp_postmeta` WHERE `post_id` = $id_bannier ") );
            $meta_value = '';
            for($d=0; $d<count($metas); $d++){
                if($metas[$d]->meta_key == '_adning_args')
                    $meta_value = $metas[$d]->meta_value;
            }
                
            $key = $metas[0]->meta_value;
            
            if($meta_value != ''){
                $getStatu = unserialize($meta_value);
                // print_r( $getStatu );
                if($getStatu['status'] == 'active'){
                    $adzones = $getStatu['adzones'][0];
                    if(isset($adzones)){
                        $urlsrc = '/admin?_dnlink='.$id_bannier.'&aid='.$adzones.'&t='.time();
                        if($getStatu['banner_content'] != ''){
                            $pop = '<a class="img-ads" href="'.$urlsrc.'" target="_blank">'.$getStatu['banner_content'].'</a>';
                        }
                        else {
                            // echo 'banner_content mkynach';
                            $pop = 'vide';
                        }
                    }
                    else 
                        // echo 'adzones mkynach';
                        $pop = 'vide';
                    
                }
                    
                else 
                    // echo 'noactive';
                    $pop = 'vide';
            }
            else 
                // echo 'NO';
                $pop = 'vide';
        }
        else 
            $pop = 'vide';
        
        return array('pop' => $pop);   
    }
    public function popup()
    {
        $pop = '';
        
        $bans = DB::select( DB::raw(" SELECT * FROM wp_posts WHERE `ID` = 12541 ") );
        // print_r( $bans );
        if(count($bans) > 0){
            $id_zone = $bans[0]->ID;
            $metas = DB::select( DB::raw(" SELECT * FROM `wp_postmeta` WHERE `post_id` = $id_zone ") );
            $meta_value = '';
            for($d=0; $d<count($metas); $d++){
                if($metas[$d]->meta_key == '_adning_args')
                    $meta_value = $metas[$d]->meta_value;
            }
                
            $key = $metas[0]->meta_value;
            
            if($meta_value != ''){
                $getStatu = unserialize($meta_value);
                // print_r( $getStatu );
                if($getStatu['status'] == 'active'){
                    
                    if(isset($getStatu['linked_banners'][0])){
                        $id_bannier = (int) $getStatu['linked_banners'][0];
                         $metasban = DB::select( DB::raw(" SELECT * FROM `wp_postmeta` WHERE `post_id` = $id_bannier ") );
                          $meta_valueban = '';
                        for($d=0; $d<count($metasban); $d++){
                            if($metasban[$d]->meta_key == '_adning_args')
                                $meta_valueban = $metasban[$d]->meta_value;
                        }
                        
                        $keyban = $metasban[0]->meta_value;
                    }else{
                        $metasban=array();
                        $meta_valueban = '';
                    }
                    
                    
                    
                   
                   
                    if($meta_valueban != ''){
                        $getStatuban = unserialize($meta_valueban);
                        // print_r( $getStatuban );
                        $urlsrc = '/admin?_dnlink='.$id_bannier.'&aid='.$id_zone.'&t='.time();
                        if($getStatuban['banner_content'] != ''){
                            $pop = '<a class="img-ads" href="'.$urlsrc.'" target="_blank">'.$getStatuban['banner_content'].'</a>';
                        }
                        else {
                            $pop = 'vide';
                        }
                    }
                    
                }
                    
                else 
                    $pop = 'vide';
            }
            else 
                $pop = 'vide';
        }
        else 
            $pop = 'vide';
        
        return array('pop' => $pop);   
    }
    public function test()
    {
        
        
            
        print_r( $this->popupzon() );
        die();
        $app = new App();
        $bans = $app->getPopup();
        // echo $bans;
        
        /*$key = $bans[0]->metas[0]->meta_value;
        if(isset($key)){
            $getStatu = unserialize($key);
            if($getStatu['status'] == 'active')
                echo 'active';
            else 
                echo 'noactive';
        }
        else 
            echo 'NO';*/
        
        
        //echo $bans['ads'][12541];
    /*   print_r($_COOKIE);
       print_r( "test");
       die();

        $app=new App();

        $bans=$app->getBanniers();*/
       //$app->inserttoLogFile($bans["infos"]);            
      /*   Storage::put('file.txt', '');*/
       /* $array="'impression'; 0; 0; 517; 'adning::id_1::banner_id,id_2::adzone_id,id_3::advertiser_id'; 21; 1555581052\n";
        $array.="'impression';0;0;517;'adning::id_1::banner_id,id_2::adzone_id,id_3::advertiser_id';24;1555581064\n";
        $array.="'click';0;0;517;'adning::id_1::banner_id,id_2::adzone_id,id_3::advertiser_id';;24;1555581070";
         Storage::prepend('file.txt',  $array);*/
       
        
        

       
   // return  $contents;
        // 1 testter l'insertion 
        // 2 mettre le cron et le tester
        // 3 inserer dans le fichier
         /*$contents = file(storage_path('app/file.txt'));
        foreach ($contents as $line) {
            print_r($line);
        }*/
    die();
          $politique = Post::taxonomy('category', 'politique')->newest()->published()
                         ->limit(6);        
        $finance = Post::taxonomy('category', 'finance-economie')->newest()->published()
                         ->limit(5);                 
        $sports = Post::taxonomy('category', 'sport')->newest()->published()
                         ->limit(6);        
        $culture = Post::taxonomy('category', 'culture')->newest()->published()
                         ->limit(5); 
        $monde = Post::taxonomy('category', 'monde')->newest()->published()
                         ->limit(6);     
        $high = Post::taxonomy('category', 'high-tech')->newest()->published()
                         ->limit(5);                         
        $events = Post::taxonomy('category', 'events')->newest()->published()
                         ->limit(5);                     
        $lifestyle = Post::taxonomy('category', 'lifestyle')->newest()->published()
                         ->limit(4);
        $videos = Post::taxonomy('category', 'videos')->newest()->published()
                         ->limit(4);
        $people = Post::taxonomy('category', 'people')->newest()->published()
                         ->limit(4);                                                       
        $posts = Post::taxonomy('category', 'actualite')->newest()->published()
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->union($events)
                        ->union($lifestyle)
                        ->union($videos)
                        ->union($people)
                        ->get();
        $app=new App();
        
        $categories=$app->getCategories();     
        $bans=$app->getBanniers(); 
        return view("test", ['posts' =>$posts,'categories' =>$categories,'bans' =>$bans["ads"], 'popup' => $this->popup()]);
    }
     public function categorie($unique_id)
    {  

        $app=new App();
        $top_posts=$app->topPosts();
        $categories=$app->getCategories();            
        $categorie=$app->getCategories($unique_id);
        if(!$categorie){
            return abort(404);        
        }    
        $posts = Post::taxonomy('category', $unique_id)->newest()->published()->paginate(9);       
        $bans=$app->getBanniers();               
        $app->inserttoLogFile($bans["infos"],1,$categorie['pub_id']);              
        return view("categorie", ['posts' =>$posts,'top_posts' =>$top_posts,'categories' =>$categories,'categorie' =>$categorie,'bans' =>$bans["ads"], 'popup' => $this->popup()]);
    }

    public function article($categorie="",$postName="",$id=0)
    {
        $app=new App();
        $categories=$app->getCategories(); 
           
        $cat=$categorie;
        $categorie=$app->getCategories($categorie);            
        $related_posts = Post::taxonomy('category', $categorie)
        ->published()
        ->newest()
        ->where("ID","!=",$id)
        ->take(4)
         ->get();    
         
         if ($cat!="events" and (!$id or $id==0 or !$categorie )){
            return abort(404);
        }
        
        if($cat=="events" and (!$id or $id==0)){
            return abort(404);
        }
       $post = Post::find($id);  

       if(!$post){
            return abort(404);        
       }
        $bans=$app->getBanniers();               
        $app->inserttoLogFile($bans["infos"],1,$categorie['pub_id']);              
        return view("article", ['post' =>$post,'categories' =>$categories,'categorie' =>$categorie,'related_posts' =>$related_posts,'cat'=>$cat,'bans' =>$bans["ads"], 'popup' => $this->popup()]);

    }

     public function inscription_view()
    {           
        $app=new App();
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();               
        return view("inscription",['categories' =>$categories,'bans' =>$bans["ads"], 'popup' => $this->popup()]);

    }
    
    public function confirmation_inscription(Request $request)
    {           
        $app=new App();
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();       
        $confirmation=$request->session()->get('confirmation');       
        if(!$confirmation){
            return redirect()->route('inscription.view');
        }
        return view("confirmation",['categories' =>$categories,'bans' =>$bans["ads"], 'popup' => $this->popup()]);

    }



         public function contact_view()
    {           
        $app=new App();
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();               
        return view("contact",['categories' =>$categories,'bans' =>$bans["ads"], 'popup' => $this->popup()]);

    }

    public function inscription_insert(Request $request)
    {

        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'alpha' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            $validatedData = $request->validate([
                'civilite' => 'alpha|required|max:10',        
                'nom' => 'alpha|required|max:255',        
                'email' => 'required|max:255|unique:newsletter',        
                'fonction' => 'required|max:255',        
                'DepartementCadre' => 'max:255',        
                'departementProfessionL' => 'max:255',        
                'departementassistanteD' => 'max:255',        
                'departementAutres' => 'max:255',        
                'date' => 'required|date|max:255',        
                'prenom' => 'alpha|required|max:255',        
                'telephone' => 'nullable|numeric',        
                'entreprise' => 'max:255',
                'ville' => 'required|max:255',
                'acceptance1' => 'required|max:255',
                'acceptance2' => 'max:255',
                'acceptance3' => 'max:255',
            ],$messages);


            if($request->ajax() and $validatedData){
                $app=new App();
                $insertion=$app->inscrption_a_la_newsletter($request);
                if($insertion){
                    $message = (string)\View::make('mail.sendmail');
                    $headers= "Content-Type: text/html; charset=UTF-8\r\n";
                    $headers.= 'From: Linformation.ma contactform@linformation.ma' . "\r\n" .
                     'X-Mailer: PHP/' . phpversion();
                    if(mail (  $request["email"] , "Inscription à la newsletter" ,  $message,$headers)){
                        $data["type"]="success";
                        $data["message"]="Votre inscription a été bien effectué !";
                        $request->session()->flash('confirmation', 'Votre inscription a été bien effectué !');
                        return $data;
                    }else{
                        $data["type"]="danger";
                        $data["message"]="error merci de ressayé plustard!";
                        return $data;
                    }
                    
                }else{
                    $data["type"]="danger";
                    $data["message"]="error merci de ressayé plustard!";
                    return $data;
                }
            }else{
                // data is not ajax
                return null;
            }
         
    }  
    
   /* public function map(){
    
         SitemapGenerator::create("https://www.linformation.ma/")
           ->writeToFile(public_path('sitemap.xml'));
    }*/

         public function contact_sendmail(Request $request)
            {
                $messages = [
                'required' => 'Le champ :attribute est obligatoire.',
                'email.unique' => 'Cette adresse email est déjà utilisée.',
                'alpha' => ' Le champ :attribute ne doit comporter que des lettre.',
                'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
                ];

                    $validatedData = $request->validate([
                        'nom' => 'alpha|required|max:255',        
                        'subject' => 'required|max:255',        
                        'message' => 'required|max:100',        
                        'email' => 'required|max:255',        
                        'telephone' => 'nullable|numeric',        
                    ],$messages);


                    if($request->ajax() and $validatedData){
                    //  $app=new App();
                        // Mail::to($request["email"])->send(new sendEmail($request["nom"],$request["subject"],$request["telephone"],$request["message"]));
                        $message= "Nom : ".$request["nom"].'<br/>';
                        $message.= "Email : ".$request["email"].'<br/>';
                        $message.= "Téléphone : ".$request["telephone"].'<br/>';
                        $message.= "Subject : ".$request["subject"].'<br/>';
                        $message.= "message : ".$request["message"].'<br/>';
                        $headers= "Content-Type: text/html; charset=UTF-8\r\n";
                        $headers.= 'From: Linformation.ma contactform@linformation.ma' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                        if(mail (  "support@le20heures.com" , $request["subject"] ,  $message,$headers)){
                            $data["type"]="success";
                            $data["message"]="Votre Message a été envoyé !";
                            return $data;
                        }else{
                            return null;
                        }
                    }else{
                        // data is not ajax
                        return null;
                    }
                
            }

            
    
    
}