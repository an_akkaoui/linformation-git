<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;
use App\Bannier;
use Carbon\Carbon;
class BanniersController extends Controller
{
   public function show_bannier($id_zone=''){
    $date = Carbon::now();
    $bannier = Bannier::where('dt_debut', '<=' ,$date)->where('dt_fin', '>=' ,$date)->where('id_zone' ,$id_zone)->orderBy('dt_debut', 'DESC')->limit(1)->get();
    if(isset($bannier) && !empty($bannier[0]->url) && !empty($bannier[0]->image)){
        
        $bannier_script = "<a rel='nowfollow' href='".$bannier[0]->url."' target='_blank'><img src='".\Voyager::image($bannier[0]->image)."' width='100%'></a>";
        
    }else{
        $bannier_script =  '';
    }
    return $bannier_script;
   }
}