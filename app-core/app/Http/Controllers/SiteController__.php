<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Corcel\Model\Post;
use App\Models\App;
use Spatie\Sitemap\SitemapGenerator;

class SiteController extends Controller
{
    function __construct()
    {    
    setlocale (LC_TIME, 'fr_FR');
    }
    
     public function index()
    {        

        $politique = Post::taxonomy('category', 'politique')->newest()->published()
                         ->limit(6);        
        $finance = Post::taxonomy('category', 'finance-economie')->newest()->published()
                         ->limit(5);                 
        $sports = Post::taxonomy('category', 'sport')->newest()->published()
                         ->limit(6);        
        $culture = Post::taxonomy('category', 'culture')->newest()->published()
                         ->limit(5); 
        $monde = Post::taxonomy('category', 'monde')->newest()->published()
                         ->limit(6);     
        $high = Post::taxonomy('category', 'high-tech')->newest()->published()
                         ->limit(5);                         
        $events = Post::taxonomy('category', 'events')->newest()->published()
                         ->limit(5);                     
        $lifestyle = Post::taxonomy('category', 'lifestyle')->newest()->published()
                         ->limit(4);
        $videos = Post::taxonomy('category', 'videos')->newest()->published()
                         ->limit(4);
        $people = Post::taxonomy('category', 'people')->newest()->published()
                         ->limit(4);                                                       
        $posts = Post::taxonomy('category', 'actualite')->newest()->published()
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->union($events)
                        ->union($lifestyle)
                        ->union($videos)
                        ->union($people)
                        ->get();
        $app=new App();
        
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();
        return view("index", ['posts' =>$posts,'categories' =>$categories,'bans' =>$bans]);
    }
    public function test()
    {           
          $post = Post::find(13640); 
                        
            print_r($post->meta);
            die();    
        $finance = Post::taxonomy('category', 'finance-economie')->newest()->published()
                         ->limit(5);                 
        $sports = Post::taxonomy('category', 'sport')->newest()->published()
                         ->limit(6);        
        $culture = Post::taxonomy('category', 'culture')->newest()->published()
                         ->limit(5); 
        $monde = Post::taxonomy('category', 'monde')->newest()->published()
                         ->limit(6);     
        $high = Post::taxonomy('category', 'high-tech')->newest()->published()
                         ->limit(5);                         
        $events = Post::taxonomy('category', 'events')->newest()->published()
                         ->limit(5);                     
        $lifestyle = Post::taxonomy('category', 'lifestyle')->newest()->published()
                         ->limit(4);
        $videos = Post::taxonomy('category', 'videos')->newest()->published()
                         ->limit(4);
        $people = Post::taxonomy('category', 'people')->newest()->published()
                         ->limit(4);                                                       
        $posts = Post::taxonomy('category', 'actualite')->newest()->published()
                        ->limit(5)
                        ->union($politique)
                        ->union($finance)
                        ->union($sports)
                        ->union($culture)
                        ->union($monde)
                        ->union($high)
                        ->union($events)
                        ->union($lifestyle)
                        ->union($videos)
                        ->union($people)
                        ->get();
        $app=new App();
        
        $categories=$app->getCategories();     
        $bans=$app->getBanniers();               
        return view("test", ['posts' =>$posts,'categories' =>$categories,'bans' =>$bans]);
    }
     public function categorie($unique_id)
    {  

        $app=new App();
        $top_posts=$app->topPosts();
        $categories=$app->getCategories();            
        $categorie=$app->getCategories($unique_id);
        if(!$categorie){
            return abort(404);        
        }    
        $posts = Post::taxonomy('category', $unique_id)->newest()->published()->paginate(9);       
        $bans=$app->getBanniers();               
        return view("categorie", ['posts' =>$posts,'top_posts' =>$top_posts,'categories' =>$categories,'categorie' =>$categorie,'bans' =>$bans]);
    }

    public function article($categorie="",$postName="",$id=0)
    {

        $app=new App();
        $categories=$app->getCategories();    
        $cat=$categorie;
        $categorie=$app->getCategories($categorie);            
        $related_posts = Post::taxonomy('category', $categorie)
        ->published()
        ->newest()
        ->where("ID","!=",$id)
        ->take(4)
        ->get();    
         
         if ($cat!="events" and (!$id or $id==0 or !$categorie )){
            return abort(404);
        }
        
        if($cat=="events" and (!$id or $id==0)){
            return abort(404);
        }
       $post = Post::find($id);  

       if(!$post){
            return abort(404);        
       }
        $bans=$app->getBanniers();               
        return view("article", ['post' =>$post,'categories' =>$categories,'categorie' =>$categorie,'related_posts' =>$related_posts,'cat'=>$cat,'bans' =>$bans]);

    }

     public function inscription_view()
    {           
        $app=new App();
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();               
        return view("inscription",['categories' =>$categories,'bans' =>$bans]);

    }



         public function contact_view()
    {           
        $app=new App();
        $categories=$app->getCategories();            
        $bans=$app->getBanniers();               
        return view("contact",['categories' =>$categories,'bans' =>$bans]);

    }

        public function inscription_insert(Request $request)
    {

        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'alpha' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            $validatedData = $request->validate([
                'civilite' => 'alpha|required|max:10',        
                'nom' => 'alpha|required|max:255',        
                'email' => 'required|max:255|unique:newsletter',        
                'fonction' => 'required|max:255',        
                'DepartementCadre' => 'max:255',        
                'departementProfessionL' => 'max:255',        
                'departementassistanteD' => 'max:255',        
                'departementAutres' => 'max:255',        
                'date' => 'required|date|max:255',        
                'prenom' => 'alpha|required|max:255',        
                'telephone' => 'nullable|numeric',        
                'entreprise' => 'max:255',
                'ville' => 'required|max:255',
                'acceptance1' => 'required|max:255',
                'acceptance2' => 'max:255',
                'acceptance3' => 'max:255',
            ],$messages);


            if($request->ajax() and $validatedData){
                $app=new App();
                $insertion=$app->inscrption_a_la_newsletter($request);
                if($insertion){
                    $message = (string)\View::make('mail.sendmail');
                    $headers= "Content-Type: text/html; charset=UTF-8\r\n";
                    $headers.= 'From: Linformation.ma contactform@linformation.ma' . "\r\n" .
                     'X-Mailer: PHP/' . phpversion();
                    if(mail (  $request["email"] , "Inscription à la newsletter" ,  $message,$headers)){
                        $data["type"]="success";
                        $data["message"]="Votre inscription a été bien effectué !";
                        return $data;
                    }else{
                        $data["type"]="danger";
                        $data["message"]="error merci de ressayé plustard!";
                        return $data;
                    }
                    
                }else{
                    $data["type"]="danger";
                    $data["message"]="error merci de ressayé plustard!";
                    return $data;
                }
            }else{
                // data is not ajax
                return null;
            }
         
    }  
    
    public function map(){
    
         SitemapGenerator::create("https://www.linformation.ma/")
           ->writeToFile(public_path('sitemap.xml'));
    }

         public function contact_sendmail(Request $request)
    {
        $messages = [
        'required' => 'Le champ :attribute est obligatoire.',
        'email.unique' => 'Cette adresse email est déjà utilisée.',
        'alpha' => ' Le champ :attribute ne doit comporter que des lettre.',
        'numeric' => ' Le champ :attribute ne doit comporter que des chiffre.',
        ];

            $validatedData = $request->validate([
                'nom' => 'alpha|required|max:255',        
                'subject' => 'required|max:255',        
                'message' => 'required|max:100',        
                'email' => 'required|max:255',        
                'telephone' => 'nullable|numeric',        
            ],$messages);


            if($request->ajax() and $validatedData){
              //  $app=new App();
                // Mail::to($request["email"])->send(new sendEmail($request["nom"],$request["subject"],$request["telephone"],$request["message"]));
                $message= "Nom : ".$request["nom"].'<br/>';
                $message.= "Email : ".$request["email"].'<br/>';
                $message.= "Téléphone : ".$request["telephone"].'<br/>';
                $message.= "Subject : ".$request["subject"].'<br/>';
                $message.= "message : ".$request["message"].'<br/>';
                $headers= "Content-Type: text/html; charset=UTF-8\r\n";
                $headers.= 'From: Linformation.ma contactform@linformation.ma' . "\r\n" .
                 'X-Mailer: PHP/' . phpversion();
                if(mail (  "support@le20heures.com" , $request["subject"] ,  $message,$headers)){
                    $data["type"]="success";
                    $data["message"]="Votre Message a été envoyé !";
                    return $data;
                }else{
                    return null;
                }
            }else{
                // data is not ajax
                return null;
            }
         
    }  
}