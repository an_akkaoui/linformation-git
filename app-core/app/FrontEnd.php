<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Voyager;
use Intervention\Image\Facades\Image;
use DateTime;

class FrontEnd extends Model
{
	public function CropImages($images = array(), $width=0, $height=0, $thumbnail_name, $month)
	{
		foreach ($images as $key => $value) {

				$infoPath = pathinfo($value->image);

				$extension = $infoPath['extension'] ?? '';

				$directory = $infoPath['dirname'] ?? '';

				// dd(public_path($value->image));

				// dd($infoPath);
				if(!empty($directory)){
					$fullpath =  '/'.$directory.'/'.$infoPath['filename'].'.'.$extension;

					$destImagePath =  '/'.$directory.'/'.$infoPath['filename'].'-'.$thumbnail_name.'.'.$extension;

					if(strpos($fullpath, $month) !== false ){
						$oldpath = storage_path($fullpath);
						$oldpath = str_replace('/app-core', '', $oldpath);

						$newpath = storage_path($destImagePath);
						$newpath = str_replace('/app-core', '', $newpath);

						// dd($oldpath);
						// dd($newpath);
						


						Image::make($oldpath)->resize($width, $height)->save($newpath);
               			echo $newpath.'--> SAVED !<br>';
               		}
				}
                
 
		}
	}
		
    public function SortPosts($posts = array())
	    {
	    	$vg = new Voyager();

	        $newsposts_categorie=array();
	        

	        foreach ($posts as $key=>$value) {
	        	$newpost = new \stdClass();
	            $newpost->title = $value->title;
	            $newpost->category_id = $value->category_id;
	            $newpost->categoryname = $value->category->name;
	            $newpost->categoryslug = $value->category->slug;
	            $newpost->seo_title = $value->seo_title;
	            $newpost->excerpt = $value->excerpt;
	            $newpost->body = $value->body;
	            $newpost->image = $value->image;
	            $newpost->slug = $value->slug;
	            $newpost->meta_description = $value->meta_description;
	            $newpost->meta_keywords = $value->meta_keywords;
	            $newpost->created_at = $value->created_at;
	            $newpost->published_at = $value->published_at;
	            $newpost->date_evenement = $value->date_evenement;
	            $newpost->source_link = $value->source_link;
	            $newpost->source_text = $value->source_text;
	            $newpost->order_post = $value->order_post;
	            $newpost->postUrl = $this->postUrl($value);
	            $newpost->categoryUrl = $this->categoryUrl($value);
	            $newpost->thumb_356x220 = $vg->image($value->thumbnail('356x220'));
	            $newpost->thumb_150x150 = $vg->image($value->thumbnail('150x150'));
	            $newpost->thumb_324x160 = $vg->image($value->thumbnail('324x160'));
	            $newpost->thumb_img_type_1 = $vg->image($value->thumbnail('img_type_1'));
	            $newpost->thumb_img_type_2 = $vg->image($value->thumbnail('img_type_2'));
	            $newpost->thumb_img_type_3 = $vg->image($value->thumbnail('img_type_3'));

	            $newpost->trimSourceLink = $this->trimSourceLink($value);

	           // if(isset( $newsposts_categorie[$value->category_id]))
	            $newsposts_categorie[$value->category_id][]=$newpost;

	        }
	        
	        // foreach dyal ta9ssim par categorie 
	        foreach ($newsposts_categorie as $key => $value) {
	        	$arrayarticle=$value;
	        	usort($arrayarticle, array($this, "sort_order"));
	        	$newsposts_categorie[$key]=$arrayarticle;
	        }

	       // dd($newsposts_categorie);

	        return $newsposts_categorie;
	    }

	public function SortPostsCat($posts = array())
	    {
	    	$vg = new Voyager();

	        $newsposts=array();
	        

	        foreach ($posts as $key=>$value) {
	            
	        	$newpost = new \stdClass();
	            $newpost->title = $value->title;
	            $newpost->category_id = $value->category_id;
	            $newpost->seo_title = $value->seo_title;
	            $newpost->excerpt = $value->excerpt;
	            $newpost->categoryslug = $value->category->slug;
	            $newpost->body = $value->body;
	            $newpost->image = $value->image;
	            $newpost->slug = $value->slug;
	            $newpost->meta_description = $value->meta_description;
	            $newpost->meta_keywords = $value->meta_keywords;
	            $newpost->created_at = $value->created_at;
	            $newpost->published_at = $value->published_at;
	            $newpost->date_evenement = $value->date_evenement;
	            $newpost->source_link = $value->source_link;
	            $newpost->source_text = $value->source_text;
	            $newpost->order_post = $value->order_post;
	            $newpost->postUrl = $this->postUrl($value);
	            $newpost->categoryUrl = $this->categoryUrl($value);
	            $newpost->cropped = $vg->image($value->thumbnail('cropped'));
	            $newpost->thumb_150x150 = $vg->image($value->thumbnail('150x150'));
	            $newpost->thumb_324x160 = $vg->image($value->thumbnail('324x160'));
	            $newpost->thumb_img_type_4 = $vg->image($value->thumbnail('img_type_4'));
	            $newpost->trimSourceLink = $this->trimSourceLink($value);

	           
	            $newsposts[]=$newpost;

	        }
	        
	        	usort($newsposts, array($this, "sort_order"));

	       // dd($newsposts);

	        return $newsposts;
	    }
	public static function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime();
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'h' => 'heure'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
    }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? 'il y a '. implode(', ', $string) : 'il y a 1 heure';
    }

	public function postUrl($post){
        return url('/').'/news/'.$post->category->slug.'/'.$post->slug.'/'.$post->id;
    }
    public function categoryUrl($post){
        return url('/').'/news/'.$post->category->slug;
    }
    public function trimSourceLink($post){

        $input = $post->source_link;
                
        $input = trim($input, '/');

                
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }

        $urlParts = parse_url($input);

                
        $domain = preg_replace('/^www\./', '', $urlParts['host']);

        return $domain;
    }
    function sort_order($a, $b)
	{
		$apublished_at = date("Y-m-d",strtotime("$a->published_at"));
		$bpublished_at = date("Y-m-d",strtotime("$b->published_at"));

	    if ($apublished_at == $bpublished_at) {
		        if ($a->order_post == $b->order_post) {
		         	return 0;
		    	}else{
		    		return ($a->order_post < $b->order_post) ? -1 : 1;
		    	}
		}elseif($apublished_at < $b->published_at) {
				 if ($a->order_post == $b->order_post) {
		         	return 0;
		    	}else{
		    		return ($a->order_post < $b->order_post) ? -1 : 1;
		    	}

		}
	}


	function convertYoutube($string) {
	    return preg_replace(
	        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
	        "<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen frameborder='0'></iframe>",
	        $string
	    );
	}



}
