<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use TCG\Voyager\Voyager;
class Zone extends Model
{
    function getZones($id_page = ""){
        $date = Carbon::now();
        //$zone = Zone::all();

        //dd($zone);
 

            
        
            $zones = DB::table('zones')
                    ->select( 'zones.id', 'zones.page_id', 'zones.status','zones.web_head_script', 'zones.web_body_script', 'zones.extracted_script_bannier', 'zones.fin_bannier', 'zones.debut_bannier')
                    ->get();

            
        
        
        $zn = array();
        
        foreach ($zones as $zone) {
            // dd($date);
            if(isset($zone->id) && ($zone->status == 1) &&  ($date >=$zone->debut_bannier) && ($date <=$zone->fin_bannier) ){
                $zn["extracted_script_bannier"][$zone->id] = $zone->extracted_script_bannier;
                $zn["status"][$zone->id] = $zone->status;
                

            }else{
                $zn["extracted_script_bannier"][$zone->id] = '';
                $zn["status"][$zone->id] = '';

            }
            
        }
        // dd($zn);
        return $zn;
    }

    function getNewsletter($id_page = ""){
        $date = Carbon::now();
        // $zone = Zone::all();

        // dd($zone);
 

            
        
            $zones = DB::table('zones')
                    ->select( 'zones.id', 'zones.page_id', 'zones.status','zones.web_head_script', 'zones.web_body_script', 'zones.extracted_script_bannier', 'zones.fin_bannier', 'zones.debut_bannier','zones.newsletter_lien')
                    ->get();

            
        
        
        $zn = array();
        
        foreach ($zones as $zone) {
            // dd($date);
            if(isset($zone->id) && ($zone->status == 1) &&  ($date >=$zone->debut_bannier) && ($date <=$zone->fin_bannier) ){
                if(isset($zone->newsletter_lien)){
                     $script_replaced = preg_replace('/<a(.*)href="([^"]*)"(.*)>/', '<a$1href="'.$zone->newsletter_lien.'"$3>', $zone->extracted_script_bannier);
                  //   dd($script_replaced);
                }
              
                $zn["extracted_script_bannier"][$zone->id] = $script_replaced;
                $zn["status"][$zone->id] = $zone->status;
                

            }else{
                $zn["extracted_script_bannier"][$zone->id] = '';
                $zn["status"][$zone->id] = '';

            }
            
        }
        // dd($zn);
        return $zn;
    }

    public function campaign(){
        return $this->belongsTo('App\Campaign', 'id_campaign');
    }
}
