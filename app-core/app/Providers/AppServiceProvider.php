<?php

namespace App\Providers;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\FormFields\SelectDropdownOverrideHandler;
use TCG\Voyager\FormFields\TextOverrideHandler;
use TCG\Voyager\FormFields\RichTextBoxOverrideHandler;
use TCG\Voyager\FormFields\DateTimeOverrideHandler;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('path.public', function() {
        //     return realpath(base_path().'/../dev');
        // });

        Voyager::addFormField(SelectDropdownOverrideHandler::class);
        Voyager::addFormField(TextOverrideHandler::class);
        Voyager::addFormField(RichTextBoxOverrideHandler::class);
        Voyager::addFormField(DateTimeOverrideHandler::class);

        
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
