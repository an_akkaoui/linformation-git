<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $message;
    public $telephone;
    public $nom;

    public function __construct($n,$sub,$tel,$msg)
    {
       $this->subject=$sub;
       $this->message=$msg;
       $this->telephone=$tel;
       $this->nom=$n;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->from('contactform@le20heures.ma')->view('mail.sendmail',["nom"=>$this->nom,"tel"=>$this->telephone,"msg"=>$this->message])->subject($this->subject);
    }
}
