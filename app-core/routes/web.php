<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// Route::get('/', 'SiteController@index')->name('accueil');

Route::get('/crop_imgs/{type}/{month}', 'FrontEndController@crop_imgs')->name('crop_imgs');

// Route::post('/dev_login', 'FrontEndController@dev_log')->name('dev_login');
// Route::get('/logout', 'FrontEndController@dev_log_out')->name('dev_logout');

Route::get('/', 'FrontEndController@index')->name('acceuil');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('/show_bannier/{id_zone}', 'BanniersController@show_bannier')->name('bannier.show');

Route::get('/feed/{page}/{number_of_items}', 'FrontEndController@generate_rss_feed')->name('feed.rss');

Route::get('/admin/subscribers/export','FrontEndController@exportCsv')->name('export.subscribers');

Route::get('/admin/zones/synchronize','FrontEndController@synchro_banniers')->name('synchronize.zones');

Route::get('/inscription-a-la-newsletter', 'FrontEndController@inscription_view')->name('inscription.view');

Route::get('/confirmation-d-inscription-a-la-newsletter', 'FrontEndController@confirmation_inscription')->name('inscription.confirmation');

Route::get('/contact', 'FrontEndController@contact_view')->name('contact.view');

Route::post('/contact', 'FrontEndController@contact_sendmail')->name('contact.sendmail');

Route::post('/sondage_vote', 'FrontEndController@sondage_vote')->name('sondage.vote');

Route::post('/inscription-a-la-newsletter', 'FrontEndController@inscription_insert')->name('inscription.ins');

Route::post('/inscription-newsletter', 'FrontEndController@inscription_newsletter')->name('inscription.newsletter');

Route::post('/inscription-newsletter-completion', 'FrontEndController@inscription_newsletter_completion')->name('inscription.newsletter.completion');

Route::get('/newsletter1', 'FrontEndController@newsletter1_view')->name('newsletter1.view');
Route::get('/alert', 'FrontEndController@alert_view')->name('alert.view');

Route::get('/news/{categorie}', 'FrontEndController@categorie')->name('categorie');

Route::get('/news/{categorie}/{postName}/{id}', 'FrontEndController@article')->name('article');

Route::get('/team', 'FrontEndController@team_view')->name('team.view');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

