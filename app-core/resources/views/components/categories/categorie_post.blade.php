@foreach($posts as $post)
<div class="col-12 bg-white">
                   <div class="cards mb-2 mt-2">
                           <div class="image-data">
                              <div class="background-image" style="background: url('{{ Voyager::image($post->image) }}') center no-repeat; background-size: cover;"></div>
                           </div>
                           <div class="post-data">
                              <a href="{{ $post->postUrl }}"><h1 class="titles-cards">{{ Str::limit($post->title, 300, '...') }}</h1></a>
                              <div class="card-icons">
                                 <a href="#" class="authors" style="color: #828080;font-family: poppins;vertical-align: middle;line-height: 22px;font-size: 12px;" >Source : {{ $post->source_text }}</a>
                                 <span class="datess"style="color: #828080;font-family: poppins;vertical-align: middle;line-height: 22px;font-size: 12px;" >{{ strftime("%e %h %Y",strtotime("$post->published_at")) }}</span>
                              </div>
                              <p class="descritions">{{ Str::limit($post->excerpt, 112, '...') }}</p>
                              <div class="cta">
                                 <a href="{{ $post->postUrl }}" style="text-decoration:none ; color:#000">Lire la suite &rarr;</a>
                              </div>
                           </div>
                        </div>
                     </div>
@endforeach