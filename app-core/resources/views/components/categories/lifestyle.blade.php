@isset($posts)
<div class="col-lg-6 col-md-6 col-sm-12">
                           <div class="p-3 border bg-white mb-3">
                              <div class="widget HTML bg-white " data-version="1" id="HTML11">
                                 <h2 class="title-{{$posts[0]->categoryslug}}"> <a style="color: #f2265d" href="/news/lifestyle">Lifestyle</a></h2>
                                 <div class="widget-content">
                                    <div class="nx">
                                       <div class="nxi">
                                          <div class="novid thmb">
                                             <a href="{{ $posts[0]->postUrl }}">
                                                <div class="nla maxres lazy" style="background: url('{{ Voyager::image($posts[0]->image) }}') center center / cover no-repeat rgb(68, 68, 68);"></div>
                                             </a>
                                             <div class="bwrap">
                                                <h3 class="heading otpx" style="height: 3rem;overflow: hidden;" ><a href="{{ $posts[0]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[0]->title, 200, '...') }}</a></h3>
                                                <div class="postinfo">
                                                   <div class="postinfo">
                                                      @php $date_pub_actu = $posts[0]->published_at; @endphp
                                                      <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="ny">
                                       @for($i=1; $i < count($posts); $i++)
                                       <div class="nyi">
                                          <div class="novid thmb">
                                             <a href="#">
                                                <div class="norw"></div>
                                                <div class="nla lazy" style="background: url('{{ Voyager::image($posts[$i]->image) }}') center center / cover no-repeat rgb(232, 232, 232);"></div>
                                             </a>
                                             <div class="bwrap">
                                                <h3 class="heading otpx" style="height: 95px;overflow: hidden;"><a href="{{ $posts[$i]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[$i]->title, 200, '...') }}</a></h3>
                                                <div class="postinfo">
                                                   @php $date_pub_actu = $posts[$i]->published_at; @endphp
                                                   <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'> {{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                                </div>
                                             </div>
                                             <div class="clear"></div>
                                          </div>
                                       </div>
                                       @endfor
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

 @endisset