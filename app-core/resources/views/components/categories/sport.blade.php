@isset($posts)


<div class="plr-inner">
                     <h1 class="title-{{$posts[0]->categoryslug}}">Sport</h1>
                     <div class="" id="Popular_Posts">
                        <div class="widget PopularPosts is-active" data-version="1" id="PopularPosts2">
                           <div class="widget-content popular-posts">
                              <div class="row">
                                 @foreach($posts as $post)
                                 <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="card mb-2 card-carts" style="height: 18rem;" >
                                        <div class="imgs" style="background-image: url('{{ $post->thumb_img_type_1 }}');height: 175px; width: 100%; background-repeat: no-repeat; background-size: cover; background-position: center;">

                                        </div>
                                       <!-- <a href="{{ $post->postUrl }}"><img src="{{ $post->thumb_img_type_1 }}" class="card-img-top" alt="..."></a> -->
                                       <div class="card-bodys"> 
                                          <a href="{{ $post->postUrl }}"><h5 style="min-height: 100px;max-height: 56px;overflow: hidden;" class="card-title">{{ Str::limit($post->title, 250, '...') }}</h5></a>
                                              <div class="postinfo" style="display:flex;justify-content: space-between;align-items: center;">
                                             @php $date_pub_actu = $post->published_at; @endphp
                                             <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                              
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 @endforeach

                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


@endisset