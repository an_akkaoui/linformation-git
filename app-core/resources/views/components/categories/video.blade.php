@isset($posts)
@if(count($posts) >= 3)
<div class="byvideo section" id="Video_Posts">
                     <div class="headinfs sectionalplrs">
                        <h2 class="title-video"> <a class="title-vid" href="/news/videos">Vidéos</a></h2>
                     </div>
                     <div>
                        <div class="widget-content">
                           <div class="v-outer">
                              <div class="vl1">
                                 <div class="vl">
                                    <div class="vid vimg">
                                       <a href="{{ $posts[0]->postUrl }}">
                                          <div class="norw"></div>
                                          <div class="maxres lazy vloz" style="background: url('{{ Voyager::image($posts[0]->image) }}') center center / cover no-repeat rgb(68, 68, 68);"></div>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="bwrap">
                                    <h3 class="heading otpx"><a href="{{ $posts[0]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[0]->title, 50, '...') }}</a></h3>
                                    <div class="post-desc">{{ Str::limit($posts[0]->excerpt, 112, '...') }}<span></span></div>
                                    @php $date_pub_actu = $posts[0]->published_at; @endphp
                                    <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <div class="vl2">
                                 <div class="vls vlsx">
                                    <div class="vl">
                                       <div class="vid vimg">
                                          <a href="{{ $posts[1]->postUrl }}">
                                             <div class="norw"></div>
                                             <div class="lazy vloz" style="background: url('{{ Voyager::image($posts[1]->image) }}') center center / cover no-repeat rgb(68, 68, 68);"></div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="bwrap">
                                       <h3 class="heading otpx"><a href="{{ $posts[1]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[1]->title, 50, '...') }}</a></h3>
                                       @php $date_pub_actu = $posts[1]->published_at; @endphp
                                       <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                    </div>
                                    <div class="clear"></div>
                                 </div>
                                 <div class="vls vlss">
                                    <div class="vl">
                                       <div class="vid vimg">
                                          <a href="{{ $posts[2]->postUrl }}">
                                             <div class="norw"></div>
                                             <div class="lazy vloz" style="background: url('{{ Voyager::image($posts[2]->image) }}') center center / cover no-repeat rgb(68, 68, 68);"></div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="bwrap">
                                       <h3 class="heading otpx"><a href="{{ $posts[2]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[2]->title, 50, '...') }}</a></h3>
                                       @php $date_pub_actu = $posts[2]->published_at; @endphp
                                       <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                    </div>
                                    <div class="clear"></div>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                  </div>

 @endif
 @endisset