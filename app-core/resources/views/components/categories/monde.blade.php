@isset($posts)

<div class="ciontentsports bg-white">
                     <div class="boxx" id="main-wrapper">
                        <div class="main section" id="main">
                           <div class="widget Blog" data-version="1" id="Blog1">
                              <h2 class="heading-title">Monde</h2>
                              <div class="vieww"></div>
                              <div class="blog-posts hfeed">
                                 <div class="row">
                                    @foreach($posts as $post)
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                       <div class="card mb-2">
                                          <img src="{{ $post->thumb_img_type_2 }}" class="card-img-top" alt="{{ Str::limit($post->title, 50, '...') }}">
                                          <div class="card-bodys" style="height:7rem">
                                             <a class="title" href="{{ $post->postUrl }}">
                                                <h5 style="max-height: 4rem;overflow: hidden;min-height: 4rem;" class="card-title">{{ Str::limit($post->title, 200, '...') }}</h5>
                                             </a>
                                              <div class="postinfo" style="display:flex;justify-content: space-between;align-items: center;">
                                             @php $date_pub_actu = $post->published_at; @endphp
                                             <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                          </div>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                 </div>
                              </div>
                              <div class="clear"></div>
                           </div>
                        </div>
                     </div>
                  </div>

@endisset