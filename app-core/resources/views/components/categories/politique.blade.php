@isset($posts)
<div class="mtabs">
                     <div class="secb section nload">
                        <div class="widget HTML" id="HTML27">
                        <h2 class="title-{{$posts[0]->categoryslug}}">Politique</h2>
                        <div class="widget-content">
                           <div class="a1 acctive">

                              <div class="nx">
                                 <div class="nxi">
                                    <div class="novid thmb">
                                       <a href="{{ $posts[0]->postUrl }}">
                                          <div class="nla maxres lazy" style="background: url('{{ Voyager::image($posts[0]->image) }}') center center / cover no-repeat rgb(68, 68, 68);"></div>
                                       </a>
                                       <div class="bwrap">
                                          <h3 class="heading otpx"><a href="{{ $posts[0]->postUrl }}" class="nlpost-title">{{ $posts[0]->title }}</a></h3>
                                          <div class="postinfo">
                                             <div class="nauthor"><span>Source : </span>{{ $posts[0]->source_text }}</div>
                                             @php $date_pub_actu = $posts[0]->published_at; @endphp
                                             <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_actu"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_actu")) }}</time>
                                          </div>
                                          <div class="post-desc">{{ Str::limit($posts[0]->excerpt, 210, '') }}<span> <a href="{{ $posts[0]->postUrl }}" style="text-decoration:none ; color: #44444452;display: flex;justify-content: flex-end;padding: 16px 0px;">Lire la suite &rarr;</a></span></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="ny">
                                 @for($i=1; $i < count($posts); $i++)
                                 <div class="nyi">
                                    <div class="novid thmb">
                                       <a href="{{ $posts[$i]->postUrl }}">
                                          <div class="norw"></div>
                                          <div class="nla lazy" style="background: url('{{ Voyager::image($posts[$i]->image) }}') center center / cover no-repeat rgb(232, 232, 232);"></div>
                                       </a>
                                       <div class="bwrap">
                                          <h3 class="heading otpx" style="height: 90px;width: 100%;overflow: hidden;"><a href="{{ $posts[$i]->postUrl }}" class="nlpost-title">{{ Str::limit($posts[$i]->title, 200, '...') }} </a></h3>
                                                <div class="postinfo" >
                                             @php $date_pub_post = $posts[$i]->published_at; @endphp
                                             <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$date_pub_post"))}}'>{{ strftime("%e %h %Y",strtotime("$date_pub_post")) }}</time>
                                             
                                             </div>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                 </div>
                                 @endfor
                                 
                              </div>
                              <div class="clear"></div>
                           </div>
                           </div>
                           <div class="b1" style="display: none;"></div>
                           <div class="c1" style="display: none;"></div>
                           <div class="d1" style="display: none;"></div>
                        </div>
                     </div>
                  </div>

@endisset