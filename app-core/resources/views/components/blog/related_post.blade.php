@foreach($posts as $post)
<div class="col-lg-4 col-md-8 col-sm-12">
                                       <div class="card bg-dark text-white mb-3">
                                          <a href=""><img src="{{ Voyager::image($post->thumbnail('img_type_3')) }}" class="card-img" alt="{{$post->title}}"></a>
                                          <div class="card-img-overlay">
                                             <a href="{{ $post->postUrl() }}"><h5 class="title-blog-releatures">{{ Str::limit($post->title, 120) }}</h5></a>
                                          </div>
                                       </div>
                                    </div>
@endforeach