<script type="text/javascript">
$(function () {
 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('.fonctions').change(function() {
    if($(this).val()=="Cadre" || $(this).val()=="Cadre supérieure"){
        $(".grouphidden").addClass("d-none");
        $(".grouphidden").find("input").val("");
        $(".DepartementC").removeClass("d-none");
    }else if($(this).val()=="Professions libérales"){
        $(".grouphidden").addClass("d-none");
        $(".grouphidden").find("input").val("");
        $(".ProfessionL").removeClass("d-none");
    }else if($(this).val()=="Assistante de direction"){
        $(".grouphidden").find("input").val("");
        $(".grouphidden").addClass("d-none");
        $(".assistanteD").removeClass("d-none");

    }else if($(this).val()=="Autres"){
        $(".grouphidden").addClass("d-none");
        $(".grouphidden").find("input").val("");
        $(".autres").removeClass("d-none");

    }else{
        $(".grouphidden").find("input").val("");
        $(".grouphidden").addClass("d-none");        
    }
}); 
    // init the validator
    // validator files are included in the download package
    // otherwise download from http://1000hz.github.io/bootstrap-validator

    // $('#contact-form').validator();


    // when the form is submitted
    $('#contact-form').on('submit', function (e) {

         $('#contact-form').find('.btn').addClass("disabled");
        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {
            var url = "{{route($url)}}"
            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                error: function (xhr) {
                   $('.response').html('<div class="alert alert-danger"><ul></ul></div><br>');
                   $.each(xhr.responseJSON.errors, function(key,value) {
                     $('.response ul').append('<li>'+value+'</li>');
                 }); 
                },
                success: function (data)
                {
                    // var url = "{{route('inscription.confirmation')}}";
                    // $(location).attr('href', url);
                    $('#contact-form').find('.btn').removeClass("disabled");
                    // data = JSON object that contact.php returns
                    // we recieve the type of the message: success x danger and apply it to the 
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable">' + messageText + '</div><br>';
                    // If we have messageAlert and messageText
                    if (messageAlert && messageText) {
                        // inject the alert to .messages div in our form
                        // $('#contact-form').find('.response').html(alertBox);

                        // empty the form

                        if(data.type=="success")
                        $('#contact-form')[0].reset();
                        $('#sucess_modal').modal('show');
                    }
                }

            });
            return false;
        }
    })
    
    
    $('#inscription-form').validator();


    // when the form is submitted
    $('#inscription-form').on('submit', function (e) {

         $('#inscription-form').find('.btn').addClass("disabled");
        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {
            var url = "{{route($url)}}"
            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                error: function (xhr) {
                   $('.response').html('<div class="alert alert-danger"><ul></ul></div><br>');
                   $.each(xhr.responseJSON.errors, function(key,value) {
                     $('.response ul').append('<li>'+value+'</li>');
                 }); 
                },
                success: function (data)
                {
                    // var url = "{{route('inscription.confirmation')}}";
                    // $(location).attr('href', url);
                    $('#inscription-form').find('.btn').removeClass("disabled");
                    // data = JSON object that contact.php returns
                    // we recieve the type of the message: success x danger and apply it to the 
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable">' + messageText + '</div><br>';
                    // If we have messageAlert and messageText
                    if (messageAlert && messageText) {
                        // inject the alert to .messages div in our form
                        $('#inscription-form').find('.response').html(alertBox);

                        // empty the form

                        if(data.type=="success")
                        document.location.href="/";
                    }
                }

            });
            return false;
        }
    })

    
});

function captcha_onclick() {
            $('#recaptchaValidator').val(1);
            $('#contact-form').validator('validate');
}
</script>