<div class="row align-items-stretch retro-layout-2">
         <div class="col-md-4">
            <a href="{{ $actualite_first->postUrl }}" class="h-entry mb-30 v-height gradient" style="background-image:url('{{ Voyager::image($actualite_first->image) }}')">
               <div class="text">
                  <div class="post-categories mb-2">
                     <span class="post-category " style="background-color: var(--actualite);">Actualité</span>
                  </div>
                  <h2>{{ $actualite_first->title }}</h2>
                  <span class="date">{{strftime("%e %h %Y",strtotime("$actualite_first->published_at")) }}</span>
               </div>
            </a>
            <a href="{{ $politique_first->postUrl }}" class="h-entry v-height gradient" style="background-image:url('{{ Voyager::image($politique_first->image) }}')">
               <div class="text">
                  <div class="post-categories mb-2">
                     <span class="post-category "style="background-color: var(--politique);">Politique</span>
                  </div>
                  <h2>{{ $politique_first->title }}</h2>
                  <span class="date">{{ strftime("%e %h %Y",strtotime("$politique_first->published_at")) }}</span>
               </div>
            </a>
         </div>
         <div class="col-md-4 mb-2">
            <a href="{{ $finance_first->postUrl }}" class="h-entry img-5 h-100 gradient" style="background-image:url('{{ Voyager::image($finance_first->image) }}')">
               <div class="text">
                  <div class="post-categories mb-2">
                     <span class="post-category "style="background-color: var(--finance-economie);">Finance & Economie</span>
                  </div>
                  <h2>{{ $finance_first->title }}</h2>
                  <span class="date">{{ strftime("%e %h %Y",strtotime("$finance_first->published_at")) }}</span>
               </div>
            </a>
         </div>
         <div class="col-md-4">
            <a href="{{ $sport_first->postUrl }}" class="h-entry mb-30 v-height gradient" style="background-image:url('{{ Voyager::image($sport_first->image) }}')">
               <div class="text">
                  <div class="post-categories mb-2">
                     <span class="post-category"style="background-color: var(--sport);">Sport</span>
                  </div>
                  <h2>{{ $sport_first->title }}</h2>
                  <span class="date">{{ strftime("%e %h %Y",strtotime("$sport_first->published_at")) }}</span>
               </div>
            </a>
            <a href="{{ $monde_first->postUrl }}" class="h-entry v-height gradient" style="background-image:url('{{ Voyager::image($monde_first->image) }}')">
               <div class="text">
                  <div class="post-categories mb-2">
                     <span class="post-category" style="background-color: var(--monde);">Monde</span>
                  </div>
                  <h2>{{ $monde_first->title }}</h2>
                  <span class="date">{{ strftime("%e %h %Y",strtotime("$monde_first->published_at")) }}</span>
               </div>
            </a>
         </div>
      </div>
        <!-- Start section banner one  -->
      <div class="row p-2">
         <div class="col-12">
            <div class="container">
               @if(isset($zones["extracted_script_bannier"][4]) && !empty($zones["extracted_script_bannier"][4]))
                        {!! $zones['extracted_script_bannier'][4] !!}
               @endif
            </div>
         </div>
      </div>
      <!-- End section banner one -->


