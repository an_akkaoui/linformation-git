<!-- Start section sidebar -->
            <div class="col-md-4 col-sm-12 mt-2" >
                  <div class="rsidebar sidewrap position-static" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 100%;">
                     <div class="theiaStickySidebar position-sticky" style="padding-top: 0px; padding-bottom: 1px; position: fixed; transform: translateY(0px); top: 0px; ">
                        <div id="sidebars">
                           <div class="sidebarone section " id="Sidebar_allPages">
                              <!-- Start section top article -->
                              <div class="widget HTML sideposts" data-version="1" id="HTML17">
                                 <h2 class="title"> <a href="" style="display: inline-block;padding: 17px 0px 0px 0px;color: #000;">Articles récents</a></h2>
                                 <div class="widget-content">
                                    <div class="postlist">
                                       <div class="opin">
                                          @foreach($top_posts as $top_post)
                                          <div class="op2 opall">
                                             <div class="novid thmb">
                                                <a href="{{ $top_post->postUrl() }}">
                                                   <div class="norw"></div>
                                                   <div class="opp" style="background: url('{{ Voyager::image($top_post->image) }}') no-repeat center;background-size: cover"></div>
                                                </a>
                                                <div class="bwrap">
                                                   <h3 class="heading sidebars" style="height: 51px;overflow: hidden;"><a href="{{ $top_post->postUrl() }}" class="oapost-title">{{ $top_post->title }}</a></h3>
                                                   <time style="height: 23px;width: 100%;overflow: hidden;font-size: 11px;" class="timeago" datetime="" title='{{ strftime("%e %h %Y",strtotime("$top_post->created_at")) }}'>{{ App\FrontEnd::time_elapsed_string($top_post->created_at) }} - {{ $top_post->category->name }}</time>
                                                </div>
                                                <div class="clear"></div>
                                             </div>
                                          </div>
                                          @endforeach
                                       </div>
                                    </div>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <!-- End section top article -->

                              <!-- Start section Evénement -->
                              <div class="widget HTML boxposts mtabs nload" data-version="1" id="HTML5">
                                 <h2 class="title">Événement</h2>
                                 <div class="widget-content">
                                    <div class="postblock">
                                       <span class="loadera" style="display: none;"><i class="fas fa-circle-notch fa-spin"></i></span>
                                       <div class="a1 acctive" style="">
                                          <div class="ffvp1 ffvp">
                                             <div class="novid thmb">
                                                <a href="{{ $evenement_article->postUrl() }}">
                                                   <div class="norw"></div>
                                                   <div class="mluvp" style="background: #ededed url('{{ Voyager::image($evenement_article->image) }}') no-repeat center;background-size: cover"></div>
                                                </a>
                                                <div class="bwrap">
                                                   <h3 class="heading otpx"><a style="font: 500 12px Poppins;" href="{{ url('/').'/news/'.$evenement_article->category->slug.'/'.$evenement_article->slug.'/'.$evenement_article->id }}">{{ ($evenement_article->title) }}</a></h3>
                                                   <div class="lauthor">{{ $evenement_article->source_text }}</div>
                                                   <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$evenement_article->published_at"))}}'>{{ strftime("%e %h %Y",strtotime("$evenement_article->published_at")) }}</time>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <!-- End section Evénement -->
                              <!-- Start section newsletter -->
                              <div class="widget HTML" data-version="1" id="HTML9">
                                 <div class="widget-content">
                                    <div class="newslatter">
                                         <form id="newsletter-form-index" role="form" action="/assets/newsletter.php" method="post" novalidate="true">
                                          <div class="follow-by-email-inner">
                                              <div class="serder">
                                                  <img style="width: 20%" src="https://linformation.ma/dev/assets/img/Icons/mail.png">
                                                  <h2 style="font: 700 30px/43px Lexend;">Newsletter</h2>
                                                <!--  <p style="margin: 0 0 10px; color: #4f4f4f; font: normal 16px/26px Lexend; font-style: normal; letter-spacing: 1px; text-transform: none; text-decoration: none; text-align: center;">Inscrivez-vous pour recevoir nos newsletters dans votre boîte mail.</p> -->
                                              </div>
                                              
                                          <div class="response"></div>
                                          <div class="form-group">
                                          <!--  <label for="exampleFormControlInput1">Email address</label> -->
                                         <!-- <a class="icons-emails"><i class="fa fa-envelope icon"></i></a> -->
                                           <input type="email" name="email" class="form-control mt-2" id="email" placeholder="E-Mail">
                                          </div>

                                          <div class="form-group mt-3">
                                           <input class="btn btn-danger" type="submit" value="Envoyer" style="display: block;width: 100%;">
                                         </div>
                                         </div>
                                       </form>
                                       <form id="newsletter-form-completion-index" role="form" action="/assets/newsletter.php" method="post" novalidate="true">
                                           <div class="follow-by-email-inner">
                                          <div class="response-index"></div>
                                          <div id="form-completion">
                                          <div class="form-group">
                                           <label for="exampleFormControlInput1">Nom complet</label>
                                           <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nom complet" name="nom_complet" required>
                                          </div>

                                          <div class="form-group">
                                           <label for="exampleFormControlInput1">Fonction</label>
                                           <select class="form-select fc fonctions" name="fonction" required>
                                             <option selected>-----</option>
                                             <option value="Cadre supérieure">Cadre supérieure</option>
                                             <option value="Cadre">Cadre</option>
                                             <option value="Direction générale">Direction générale</option>
                                             <option value="Professions libérales">Professions libérales</option>
                                             <option value="Assistante de direction">Assistante de direction</option>
                                             <option value="Etudiant(e)">Etudiant(e)</option>
                                             <option value="Retraité">Retraité</option>
                                             <option value="Employé (e) de bureau">Employé (e) de bureau</option>
                                             <option value="Femme au foyer">Femme au foyer</option>
                                             <option value="Autres">Autres</option>
                                          </select>
                                          </div>

                                          <div class="form-group DepartementC d-none grouphidden">
                                           <label for="exampleFormControlInput1">Departement</label>
                                           <select class="form-select fc" name="DepartementCadre">
                                             <option selected>-----</option>
                                             <option value="Direction Financière">Direction Financière</option>
                                             <option value="Direction Juridique">Direction Juridique</option>
                                             <option value="Direction de la Communication">Direction de la Communication</option>
                                             <option value="Direction Générale">Direction Générale</option>
                                             <option value="Direction Ressources Humaines">Direction Ressources Humaines</option>
                                             <option value="Direction Informatique">Direction Informatique</option>
                                             <option value="Direction logistique">Direction logistique</option>
                                             <option value="Direction Marketing">Direction Marketing</option>
                                             <option value="Autres">Autres</option>
                                          </select>
                                          </div>

                                          <div class="form-group ProfessionL d-none grouphidden">
                                           <label for="exampleFormControlInput1">Departement</label>
                                           <select class="form-select fc" name="departementProfessionL">
                                             <option selected>-----</option>
                                             <option value="Architecte">Architecte</option>
                                             <option value="Avocat">Avocat</option>
                                             <option value="Administrateur judiciaire">Administrateur judiciaire</option>
                                             <option value="Expert-comptable">Expert-comptable</option>
                                             <option value="Médecin">Médecin</option>
                                             <option value="Notaire">Notaire</option>
                                             <option value="Autres">Autres</option>
                                          </select>
                                          </div>

                                          <div class="form-group assistanteD d-none grouphidden">
                                           <label for="exampleFormControlInput1">Departement</label>
                                           <select class="form-select fc" name="departementassistanteD">
                                             <option selected>-----</option>
                                             <option value="Assistante / secrétaire">Assistante / secrétaire</option>
                                             <option value="Assistante de département">Assistante de département</option>
                                             <option value="Assistante de direction">Assistante de direction</option>
                                             <option value="Office Manager">Office Manager</option>
                                             <option value="Autres">Autres</option>
                                          </select>
                                          </div>

                                          <div class="form-group autres d-none grouphidden">
                                           <label for="exampleFormControlInput1" class="form-label">Autre</label>
                                             <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Autres ..." name="departementAutres">  
                                          </div>



                                          <div class="form-group">
                                           <label for="exampleFormControlInput1">Ville</label>
                                           <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Ville" name="ville"  required>
                                          </div>

                                          <div class="form-group">
                                           <label for="exampleFormControlInput1">Age</label>
                                           <select class="form-select fc" name="age" required>
                                             <option selected>-----</option>
                                             <option value="18 - 25">18 - 25</option>
                                             <option value="25 - 35">25 - 35</option>
                                             <option value="35 - 45">35 - 45</option>
                                             <option value="45+">45+</option>
                                          </select>
                                          </div>



                                          <input type="hidden" name="email" value="" id="inp_email">

                                          <div class="form-group mt-3">
                                           <input class="btn btn-danger" type="submit" value="Envoyer">
                                         </div>
                                         </div>
                                      </div>
                                       </form>
                                    </div>
                                      
                                    
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <!-- End section newsletter -->
                              <div class="widget HTML" data-version="1" id="HTML20">
                              <div class="banner-sidabar">
                                                @if(isset($zones['extracted_script_bannier'][15]) && !empty($zones['extracted_script_bannier'][15]))
	                <div style="max-width: 300px; max-height: 600px;margin:auto;">
            			{!!$zones['extracted_script_bannier'][15]!!}
	                </div>
	                @endif
                              </div>
                              </div>
                                                                <!-- Start section Meteo -->
                              <div class="widget HTML boxposts mtabs nload " data-version="1" id="HTML5">
                                 <h2 class="title">Météo</h2>
                                 <div class="widget-content">
                                    <div class="widget-content mb-5">
                                    <div class="postblock ">
                                       <span class="loadera" style="display: none;"><i class="fas fa-circle-notch fa-spin"></i></span>
                                       <div class="a1 acctive" style="">
                                          <div class="ffvp1 ffvp">
                                             <div class="novid thmb">
                                                <a href="{{ $meteo_article->postUrl() }}">
                                                   <div class="norw"></div>
                                                   <div class="mluvp" style="background: #ededed url('{{ Voyager::image($meteo_article->image) }}') no-repeat center;background-size: cover">
                        <span class="post-category " style="background-color: #e1ad62;color: #fff;position: relative;top: 7px;left: 8px;">Metéo</span>
                                                   </div>
                                                </a>
                                                <div class="bwrap">
                                                   <h3 class="heading otpx"><a style="font: 500 12px Poppins;" href="{{ $meteo_article->postUrl() }}">{{ ($meteo_article->title) }}</a></h3>
                                                   <div class="lauthor">{{ $meteo_article->source_text }}</div>
                                                   <time class="timeago" datetime='{{strftime("%Y-%m-%dT%H:%M:%S+00:00",strtotime("$meteo_article->published_at"))}}'>{{ strftime("%e %h %Y",strtotime("$evenement_article->published_at")) }}</time>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <!-- End section Meteo -->
                
                              <!-- Start section Social media -->
                              <div class="widget HTML" data-version="1" id="HTML20">
                                 <h2 class="title"> <a style="color: #000;" href="#">Social media</a></h2>
                                 <div class="widget-content">
                                    <div class="sicon"><a href="https://www.facebook.com/linformation.ma/"><i class="fab fa-facebook-f"></i><span>Facebook</span></a></div>
                                    <div class="sicon"><a href="#"><i class="fab fa-pinterest-p"></i><span>Pinterest</span></a></div>
                                    <div class="sicon"><a href="https://www.instagram.com/linformation.ma/"><i class="fab fa-instagram"></i><span>Instagram</span></a></div>
                                 </div>
                                 <div class="clear"></div>
                              </div>
                              <!-- End section Social media -->
                              <!-- Start section sondage  -->
                              <div class="widget-content">
                                  <h2 class="title"> <a style="color: #000;" href="#">Sondage</a></h2> 
                                  <div>
                                      <form role="form" id="sondage-form" method="post" action="sondage.php" novalidate="true">
                                          
                                      <div class="follow-by-email-inner" style="padding: 28px 7px 26px !important;">
                                          <div class="response_sonage"></div>
                                        <h5 style="line-height: 20px;padding-bottom: 12px;font-size: 14px;word-break: break-word;text-align: justify;">{{ $sondage->question }}</h5>
                                    
                                    <div class="form-check form-check-inline">
                                        
                                      <input class="form-check-input" type="radio" name="rep1" id="Excelent" value="1">
                                      <label class="form-check-label title" for="Excelent">{{ $sondage->rep1 }}</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="rep2" id="Moyen" value="1">
                                      <label class="form-check-label title" for="inlineRadio2">{{ $sondage->rep2 }}</label>
                                         </div>
                                     <div class="form-check form-check-inline">
                                      <input class="form-check-input" type="radio" name="rep3" id="Mauvai" value="1">
                                      <label class="form-check-label title" for="inlineRadio2">{{ $sondage->rep3 }}</label>
                                         </div>
                                                   <div class="form-group mt-3" style="display: flex;justify-content: center;align-items: center;padding-right: 37px;">
                                    <button class="btn btn-danger" type="submit">Votez</button>
                                    </form>
                                </div>
                                    </div>
                           
                                </div>
                               
                                  </div>
                                
                            <!-- End section sondage  -->
                           </div>
                           
                        </div>
                     </div>
                  </div>
               </div>

               
               
            <!-- End section sidebar -->


