<!-- Start section Header -->
   <div class="top-header">
      <a href="{{url('/')}}"><img class="logo" src="{{ asset('assets/img/logo-linformation.png') }}" alt=""></a>
   </div>
   <nav class="navbar navbar-expand-lg navbar-light menu">
      <div class="container">
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
               @php $i=0; @endphp

            @for($i=0; $i < 7; $i++)
               <li class="nav-item">
                  <a class="nav-link {{ $categories[$i]->class }}" href="{{route('categorie',['categorie'=>$categories[$i]['slug']])}}">{{ $categories[$i]->name }}</a>
               </li>
            @endfor



               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Autres
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                     @for($i=8; $i < 11; $i++)
                        <li><a class="dropdown-item {{ $categories[$i]->class }}" href="{{route('categorie',['categorie'=>$categories[$i]['slug']])}}">{{ $categories[$i]->name }}</a></li>
                        @if($i < 10)
                        <li>
                           <hr class="dropdown-divider">
                        </li>
                        @endif

                        @endfor

                  </ul>
               </li>
            </ul>
            <div class="d-flex">
               <a class="items" href="{{ route('contact.view') }}"><i class="fas fa-address-book"></i> Contact</a>
               <a class="items" href="{{ route('inscription.view') }}"><i class="fas fa-envelope"></i> Inscrivez-vous</a>
            </div>
         </div>
      </div>
   </nav>
   <!-- End section Header -->


