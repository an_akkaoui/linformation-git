<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="keywords" content="@if(View::hasSection('meta_keywords'))@yield('meta_keywords')@else linformation.ma, Maroc, Infos, Site infos, Actualité, Politique,Finance & Economie, Sport, Culture, Monde, High Tech, LifeStyle, Vidéos, People @endif">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:title"              content="@hasSection('seo_title')@yield('seo_title')@else @yield('title') @endif - {{ setting('site.title') }}" />
       @if(View::hasSection('description'))
           <meta property="og:description"              content="@yield('description')" />
       @endif
       @if(View::hasSection('image'))
           <meta property="og:image"              content="@yield('image')" />
       @endif

       @if(View::hasSection('description'))
        <meta name="description" content="@yield('description')">
       @endif
    <!--Start JS Section -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145246525-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-145246525-1');
    </script>
    
    <script type="text/javascript" src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=6273d7679d3f9e001262d403&product=inline-share-buttons' async='async'></script>
    <!-- End JS Section -->

   <!-- Start stylesheets bootsrap -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
   <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
   <!-- Start stylesheets bootsrap -->

   <link rel="stylesheet" href="{{ asset('assets/css/style.css?=10') }}">

   <link href="{{ asset('assets/css/magnific-popup.css')}}" rel="stylesheet">

   <!-- Favicons-->
    <link rel="icon" href="{{url('/')}}/assets/img/icon32x32.jpeg" sizes="32x32">
    <link rel="icon" href="{{url('/')}}/assets/img/icon192x192.jpeg" sizes="192x192">

   <title>@yield('title') - {{ setting('site.title') }}</title>
</head>