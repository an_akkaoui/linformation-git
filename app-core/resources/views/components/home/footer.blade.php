 <!-- Start Section  footer  -->
   <div class="sectionfoot">
      <div class="footb">
         <div class="footercolm">
            <div class="footera footerall">
               <div class="footerone section" id="Footer_Column_One">
                  <div class="widget HTML" data-version="1" id="HTML1">
                     <div class="widget-content">
                        <img src="{{ asset('assets/img/logo-linformation-footer.png') }}" style="margin-bottom: 9px;display: block;width: 100%">
                        <p class="p-foot">Suivez l'actualité nationale et internationale tous les soirs avec linformation.ma</p>
                        <div class="flinks">
                           <a href="#"><i class="far fa-envelope"></i>info@wibday.com</a>
                           <a href="#"><i class="fas fa-headphones"></i>+212 684 101 010</a>
                        </div>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
            </div>
            <div class="footerd footerall">
               <div class="footerfour section" id="Footer_Column_Four">
               </div>
            </div>
            <div class="footerd footerall">
               <div class="footerfour section" id="Footer_Column_Four">
               </div>
            </div>
            <div class="footerd footerall">
               <div class="footerfour section" id="Footer_Column_Four">
                  <div class="widget Label" data-version="1" id="Label3">
                     <h2></h2>
                     <div class="widget-content list-label-widget-content">
                        <ul>
                           <li>
                              <a dir="ltr" href="{{ route('contact.view') }}"> Se désabonner</a>
                              <span dir="ltr"><i class="far fa-bell"></i></span>
                           </li>
                           <li>
                              <a dir="ltr" href="https://linformation.ma/assets/pdf/note-legale.pdf">Note légale</a>
                              <span dir="ltr"><i class="far fa-clipboard"></i></span>
                           </li>
                           <li>
                              <a dir="ltr" href="{{ route('contact.view') }}">Contactez-nous</a>
                              <span dir="ltr"><i class="far fa-address-book"></i></span>
                           </li>
                            <li>
                              <a dir="ltr" href="{{ route('team.view') }}">Équipe de travail</a>
                              <span dir="ltr"><i class="fa fa-users"></i></span>
                           </li>
                           <li>
                              <a target="_blank" dir="ltr" href="https://linformation.ma/assets/pdf/presentation.pdf">Kit Media</a>
                              <span dir="ltr"><i class="far fa-file-pdf"></i></span>
                           </li>
                        </ul>
                        <div class="clear"></div>
                     </div>
                  </div>
               </div>
               <div class="cndp">
                  <img width="28%" src="{{ asset('assets/img/cndp.png') }}" alt="cndp">
                  <p id="acc">Accréditation N° : D-NL-189/2020</p>
                  <p id="acc" style="line-height:30px;">Dossier presse : 2022/02</p>
                  
               </div>
            </div>
            <div class="clear"></div>
         </div>
      </div>
      <div class="footc">
         <div class="footca">
            <div class="footcopy">
               Copyright ©Linformation.ma | Tous Droits Résérvés
            </div>
            <div class="footdesign">
               Développé par    <a target="_blank" href="https://wibday.com/"> <i class="fas fa-barcode"></i> WibDay</a>  
            </div>
            <div class="clear"></div>
         </div>
      </div>
   </div>