@if ($paginator->hasPages())
    <div class="col-lg-12">
      <div class="row text-center p-4">
        <div class="col-md-12">
         <div class="custom-pagination">
        {{-- Previous Page Link --}}
        <!-- @if ($paginator->onFirstPage())
        <span aria-label="@lang('pagination.previous')">&lsaquo;</span>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
        @endif -->

        @if($paginator->currentPage() > 4)
            <a href="{{ $paginator->url(1) }}">1</a>
        @endif
        @if($paginator->currentPage() > 5)
            <span>...</span>
        @endif


        {{-- Array Of Links --}}
        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 3 && $i <= $paginator->currentPage() + 3)
                @if ($i == $paginator->currentPage())
                    <span>{{ $i }}</span>
                @else
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                @endif
            @endif
        @endforeach

        @if($paginator->currentPage() < $paginator->lastPage() - 4)
            <span>...</span>
        @endif

        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
        @endif


        <!-- {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
        @else
         <span class="page-link disabled" aria-hidden="true" aria-label="@lang('pagination.next')">&rsaquo;</span>
        @endif -->
     </div>
    </div>
    </div>
 </div>
@endif
