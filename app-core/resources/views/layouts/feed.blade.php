<?=
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
@php
setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
$date = (strftime("%A %d %B %Y, %T")); 
@endphp
<rss version="2.0">
    <channel>
        <title><![CDATA[ Linformation.ma ]]></title>
        <link><![CDATA[ {{ url('/') }}/feed ]]></link>
        <description><![CDATA[ {{ setting('site.title') }} ]]></description>
        <language>fr</language>
        <pubDate>{{ $date }}</pubDate>

        @foreach($posts as $post)
            <item>
                <guid>{{ $post->id }}</guid>
                <title>{{ $post->title }}</title>
                <link>{{ $post->postUrl() }}</link>
                <description>{!! $post->excerpt !!}}</description>
                <category>{{ $post->category->name }}</category>
                <author>{{ $post->author_id  }}</author>
                <source>{{ $post->source_text }}</source>
                <pubDate>{{ $post->published_at }}</pubDate>
            </item>
        @endforeach
    </channel>
</rss>