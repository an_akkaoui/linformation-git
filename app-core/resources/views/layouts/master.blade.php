@component('components.home.head')
@endcomponent

<body>

      <!-- Start Section Header -->
      @component('components.home.header', ['categories' => $categories, 'zones' => $zones])
      @endcomponent

        <!-- Start section habillage -->
        @component('components.home.habillage', ['zones' => $zones])
        @endcomponent
        <!-- End section habillage -->

        <div class="pt-3 ">
        @if(Request::is('/')) 
  
         <!-- Start Section slider -->
               
         
            @component('components.home.slider' , ['actualite_first' => $posts[1][0], 'politique_first' => $posts[2][0], 'finance_first' => $posts[3][0], 'sport_first' => $posts[4][0], 'monde_first' => $posts[6][0], 'zones' => $zones ])
            @endcomponent
         
         @else

         @if(isset($categorie->pub_id) && !empty($categorie->pub_id) && ($categorie->pub_id != 15) && !isset($zones["extracted_script_bannier"][1]) && empty($zones["extracted_script_bannier"][1]))
         @if($categorie->pub_id != 4)
         <!-- Start section banner one  -->
          <div class="row p-2">
             <div class="col-12">
                <div class="container" style="max-width:800px;margin: 20px auto">
                   {!! $zones['extracted_script_bannier'][$categorie->pub_id] !!}
                </div>
             </div>
          </div>
        <!-- End section banner one -->

         @else
         <!-- Start section banner one  -->
         <div class="row p-2">
             <div class="col-12">
                <div class="container" style="max-width:920px;margin: 20px auto">
                   {!! $zones['extracted_script_bannier'][$categorie->pub_id] !!}
                </div>
             </div>
          </div>
        <!-- End section banner one -->
         @endif

         @endif
         
         
         @endif

         <!-- End Section slider -->


        <div class="container">
         <div class="row">
        <!-- Content ================================================== -->
        @yield('content')
        <!-- End Content =============================================== -->
              
               <!-- Start Section sidebar -->
               @component('components.home.side-bar', ['top_posts' => $top_posts, 'evenement_article' => $events[0], 'meteo_article' => $meteo[0], 'zones' => $zones, 'sondage' => $sondage])
               @endcomponent
        
        </div>
        </div>
            <div style="position: relative; overflow: visible; box-sizing: border-box; min-height: 100%;left: 5%;">
              <a href="#" class="icons-top"><i class="fas fa-long-arrow-alt-up"></i></a>  
            </div>
            
        </div>
            
            <!-- End Section sidebar -->

            <!-- Start Section Footer -->
            @component('components.home.footer')
            @endcomponent
            <!-- End Section Footer -->
        

  
   <!-- COMMON SCRIPTS -->
   @yield('all_scripts')
   <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
   
      <!-- js jquery bootstrap -->
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


  
   <!-- End section slider  -->
   <!-- Start cdn jquery -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <!-- End cdn jquery -->

   <script type="text/javascript" src="{{ asset('assets/js/jquery.cookie.js')}}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/common_scripts_min.js') }}"></script>

   <script type="text/javascript">                                         
      $(document).ready(function($) {
      
        $.ajaxSetup({
              headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });

        if( $.cookie('showOnceADay') ){
            //it is still within the day
            //hide the div
        } else {
        
            //either cookie already expired, or user never visit the site
            //create the cookie
            
             var date = new Date();
             var minutes = 120;
             date.setTime(date.getTime() + (minutes * 60 * 1000));
             $.cookie('showOnceADay', 'showOnceADay', { expires: date });

            //and display the div
            
            

            @php
            $html = file_get_contents(('https://admanager.linformation.ma/adserve.php?f=33'));
            
            $popup = $zones['extracted_script_bannier'][14];
            @endphp
            
            @if (preg_match('/"([^"]+)"/', $html, $m)) {
                var popup = '{!! $popup !!}';

                $.magnificPopup.open({
                items: { src: "<div class='publicite'><div class='imgpub' id='img_pub' style='width:100%;max-width:800px;max-height:400px;'>"+popup+"<button title='Close (Esc)' type='button' class='mfp-close'>×</button></div></div>"
                    }, 
                type: 'inline'
            }, 0);
        
        
            } @else {
                
                $.magnificPopup.open({
                items: { src: "<div class='publicite'> <div class='imgpub' id='img_pub' style='width:100%;max-width:800px;'> <div class='gl-popup'> <div class='row'> <div class='col-lg-6 col-xs-12'> <div class='text'> <h3> Inscrivez vous à la newsletter Linformation.ma </h3> </div> <div class='response-popup'></div> <form id='newsletter-form-popup' role='form' action='/assets/newsletter.php' method='post' novalidate='true'> <div class='form-group d-flex'> <input type='email' name='email' id='email' class='form-control' placeholder='votre email' required> <button type='submit' class='btn '>Valider</button> </div> <div class='form-group checkbox'> <label id='lb1'><input type='checkbox' name='remember' required> J’ai lu et j’accepte <a style='color: #ac0000;' href='https://www.linformation.ma/assets/pdf/note-legale.pdf'> la note légale Linformation.ma</a> , notamment la mention relative à la protection des données personnelles.</label> </div> </form> </div> <div class='col-lg-6 col-xs-12'> <div class='img-block'> <img src='/assets/img/linformation.png' alt='subscribe icon'> </div> </div> </div> </div> <button title='Close (Esc)' type='button' class='mfp-close'>×</button> </div> </div>"                 
                }, 
                type: 'inline'
            }, 0);
            
            }


            @endif
            
            //// old popup
                  

        }
        
        
        $(function () {
      
          $('#newsletter-form-completion-index').hide();
        
        // init the validator
        // validator files are included in the download package
        // otherwise download from http://1000hz.github.io/bootstrap-validator
        
        $('#newsletter-form-popup').validator();
        
        
        // when the form is submitted
        $('#newsletter-form-popup').on('submit', function (e) {
        
             $('#newsletter-form-popup').find('.btn').addClass("disabled");
            // if the validator does not prevent form submit
            if (!e.isDefaultPrevented()) {
        
                var url = "{{route('inscription.newsletter')}}"
                
                // POST values in the background the the script URL
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    error: function (xhr) {
                       $('.response-popup').html('<div class="alert alert-danger"><ul></ul></div');
                       $.each(xhr.responseJSON.errors, function(key,value) {
                         $('.response-popup ul').append('<li>'+value+'</li>');
                     }); 
                    },
                    success: function (data)
                    {
                        
                        $('#newsletter-form-popup').find('.btn').removeClass("disabled");
                        $('.response-popup').html('<div>'+data.message+'</div>');
        
                            
        
                            if(data.type=="success")
                            email = $('#email').val();
                            $('#newsletter-form-index')[0].reset();
                            
                            $('#newsletter-form-completion').validator();

                            $.magnificPopup.close()

                            $.magnificPopup.open({
                            items: { src: "<div class='publicite'> <div class='imgpub' id='img_pub' style='width:100%;max-width:800px;'> <div class='gl-popup'> <div class='row'> <div class='response-popup'></div> <div class='col-lg-6 col-xs-12'> <div class='text'> <h3>Inscrivez vous sur notre newsletter </h3> </div>  <form id='newsletter-form-completion' role='form' action='/assets/newsletter.php' method='post' novalidate='true'> <div class='form-group has-error has-danger'> <label for='nom'>Nom complet*</label> <input type='text' name='nom_complet' class='nom form-control' placeholder='Nom complet' required=''> </div> <input type='hidden' name='email' value='"+email+"'> <div class='form-group'> <label for='contact-subject'>Fonction*</label> <select name='fonction' id='fonctions' class='contact-subject form-control' required='required' data-error='Champ obligatoire'><option value=''>---</option><option value='Cadre'>Cadre</option><option value='Cadre sup'>Cadre sup</option><option value='Direction générale'>Direction générale</option><option value='Professions libérales'>Professions libérales</option><option value='Assistante de direction'>Assistante de direction</option><option value='Femme au foyer'>Femme au foyer</option><option value='Employé(e)'>Employé(e)</option><option value='Autres'>Autres</option></select> <div class='help-block with-errors'></div> </div> <div class='form-group hidden grouphidden DepartementC'> <div> <label> Departement* </label> <select name='DepartementCadre' class='contact-subject form-control'><option value=''>---</option><option value='Direction Financière'>Direction Financière</option><option value='Direction Juridique'>Direction Juridique</option><option value='Direction de la Communication'>Direction de la Communication</option><option value='Direction Générale'>Direction Générale</option><option value='Direction Commerciale'>Direction Commerciale</option><option value='Direction Ressources Humaines'>Direction Ressources Humaines</option><option value='Direction Informatique'>Direction Informatique</option><option value='Direction logistique'>Direction logistique</option><option value='Direction Marketing'>Direction Marketing</option><option value='Autres'>Autres</option></select> <div class='help-block with-errors'></div> </div> </div> <div class='form-group hidden grouphidden ProfessionL'> <div> <label> Departement* </label> <select name='departementProfessionL' class='contact-subject form-control'><option value=''>---</option><option value='Architecte'>Architecte</option><option value='Avocat'>Avocat</option><option value='Administrateur judiciaire'>Administrateur judiciaire</option><option value='Expert-comptable'>Expert-comptable</option><option value='Médecin'>Médecin</option><option value='Notaire'>Notaire</option><option value='Autre'>Autre</option></select> </div> </div> <div class='form-group hidden grouphidden assistanteD'> <div> <label> Departement*</label> <select name='departementassistanteD' class='contact-subject form-control'><option value=''>---</option><option value='Assistante / secrétaire'>Assistante / secrétaire</option><option value='Assistante de département'>Assistante de département</option><option value='Assistante de direction'>Assistante de direction</option><option value='Office Manager'>Office Manager</option><option value='Autres'>Autres</option></select> <div class='help-block with-errors'></div> </div> </div> <div class='form-group hidden grouphidden autres'> <div> <label> Autres*</label> <input type='text' class='contact-subject form-control' name='departementAutres' value='' size='40'> </div> </div> <div class='form-group has-error has-danger'> <label for='ville'>Ville*</label> <input type='text' name='ville' class='ville form-control' placeholder='Ville' required=''> </div> <div class='form-group has-error has-danger'> <label for='age'>Age*</label> <select class='form-select' name='age' required> <option selected>-----</option> <option value='18 - 25'>18 - 25</option> <option value='25 - 35'>25 - 35</option> <option value='35 - 45'>35 - 45</option> <option value='45+'>45+</option> </select> <button type='submit' class='btn disabled' style='margin-top: 20px;margin-left: 7px;'>Valider</button> </div> </form></div><div class='col-lg-6 col-xs-12'> <div class='img-block'> <img src='/assets/img/linformation.png' alt='subscribe icon'> </div> </div> </div> <button title='Close (Esc)' type='button' class='mfp-close'>×</button> </div> </div></div>"                 
                            }, 
                            type: 'inline'
                        }, 0);
                        $('#newsletter-form-completion').validator();
      
                
                            // when the form is submitted
                            $('#newsletter-form-completion').on('submit', function (e) {
                        
                                 $('#newsletter-form-completion').find('.btn').addClass("disabled");
                                // if the validator does not prevent form submit
                                if (!e.isDefaultPrevented()) {
                        
                                    var url = "{{route('inscription.newsletter.completion')}}"
                                    
                                    // POST values in the background the the script URL
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: $(this).serialize(),
                                        error: function (xhr) {
                                           $('.response-popup').html('<div class="alert alert-danger"><ul></ul></div><br>');
                                           $.each(xhr.responseJSON.errors, function(key,value) {
                                             $('.response-popup ul').append('<li>'+value+'</li>');
                                         }); 
                                        },
                                        success: function (data)
                                        {
                                            
                                            $('#newsletter-form-completion').find('.btn').removeClass("disabled");
                                            $('.response-popup').html('<div class="alert alert-'+data.type+'">'+data.message+'</div>');
                        
                                                
                        
                                                if(data.type=="success")
                                                $('#newsletter-form-completion')[0].reset();
                                                $.magnificPopup.close();

                                                
                                                
                                            
                                        }
                        
                                    });
                                    return false;
                                }
                            })
                        
                    }
        
                });
                return false;
            }
        })
        
        
        $('#newsletter-form-index').validator();
        
        
        // when the form is submitted
        $('#newsletter-form-index').on('submit', function (e) {
        
             $('#newsletter-form-index').find('.btn').addClass("disabled");
            // if the validator does not prevent form submit
            if (!e.isDefaultPrevented()) {
        
                var url = "{{route('inscription.newsletter')}}"
                
                // POST values in the background the the script URL
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    error: function (xhr) {
                       $('.response').html('<div class="alert alert-danger"><ul></ul></div><br>');
                       $.each(xhr.responseJSON.errors, function(key,value) {
                         $('.response ul').append('<li>'+value+'</li>');
                     }); 
                    },
                    success: function (data)
                    {
                        
                        $('#newsletter-form-index').find('.btn').removeClass("disabled");
                        $('.response').html('<div class="alert alert-'+data.type+'">'+data.message+'</div');
        
                            // empty the form
        
                            if(data.type=="success")
                            email = $('#email').val();
                            $('#newsletter-form-index')[0].reset();
                            $('#newsletter-form-index').hide();
      
      
                            $('#newsletter-form-completion-index').show();
                            $('#inp_email').val(email);
        
                            $('#newsletter-form-completion-index').validator();
      
                            $('.fonctions').change(function() {
                                if($(this).val()=="Cadre" || $(this).val()=="Cadre supérieure"){
                                    $(".grouphidden").addClass("d-none");
                                    $(".grouphidden").find("input").val("");
                                    $(".DepartementC").removeClass("d-none");
                                }else if($(this).val()=="Professions libérales"){
                                    $(".grouphidden").addClass("d-none");
                                    $(".grouphidden").find("input").val("");
                                    $(".ProfessionL").removeClass("d-none");
                                }else if($(this).val()=="Assistante de direction"){
                                    $(".grouphidden").find("input").val("");
                                    $(".grouphidden").addClass("d-none");
                                    $(".assistanteD").removeClass("d-none");
                            
                                }else if($(this).val()=="Autres"){
                                    $(".grouphidden").addClass("d-none");
                                    $(".grouphidden").find("input").val("");
                                    $(".autres").removeClass("d-none");
                            
                                }else{
                                    $(".grouphidden").find("input").val("");
                                    $(".grouphidden").addClass("d-none");        
                                }
                            }); 
        
                            $('#newsletter-form-completion-index').on('submit', function (e) {
                        
                                 $('#newsletter-form-completion-index').find('.btn').addClass("disabled");
                                // if the validator does not prevent form submit
                                if (!e.isDefaultPrevented()) {
                        
                                    var url = "{{route('inscription.newsletter.completion')}}"
                                    
                                    // POST values in the background the the script URL
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: $(this).serialize(),
                                        error: function (xhr) {
                                           $('.response-index').html('<div class="alert alert-danger"><ul></ul></div');
                                           $.each(xhr.responseJSON.errors, function(key,value) {
                                             $('.response-index ul').append('<li>'+value+'</li>');
                                         }); 
                                        },
                                        success: function (data)
                                        {
                                            
                                            $('#newsletter-form-completion-index').find('.btn').removeClass("disabled");
                                            $('.response-index').html('<div class="alert alert-'+data.type+'">'+data.message+'</div><br>');
                        
                                                // empty the form
                        
                                                if(data.type=="success")
                                                $('#form-completion').hide();
                                        }
                        
                                    });
                                    return false;
                                }
                            })
                        
                    }
        
                });
                return false;
            }
        })
        
        });
        
        $('#sondage-form').on('submit', function (e) {

         $('#sondage-form').find('.btn').addClass("disabled");
        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {
            var url = "{{route('sondage.vote')}}"
            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                error: function (xhr) {
                   $('.response_sonage').html('<div class="alert alert-danger"><ul></ul></div><br>');
                   $.each(xhr.responseJSON.errors, function(key,value) {
                     $('.response_sonage ul').append('<li>'+value+'</li>');
                 }); 
                },
                success: function (data)
                {
                    // var url = "{{route('inscription.confirmation')}}";
                    // $(location).attr('href', url);
                    $('#sondage-form').find('.btn').removeClass("disabled");
                    // data = JSON object that contact.php returns
                    // we recieve the type of the message: success x danger and apply it to the 
                    var messageAlert = 'text-' + data.type;
                    var messageText = data.message;

                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="' + messageAlert + '">' + messageText + '</div><br>';
                    // If we have messageAlert and messageText
     
                    if (messageAlert && messageText) {
                        // inject the alert to .messages div in our form
                        $('#sondage-form').find('.response_sonage').html(alertBox);

                        // empty the form

                        if(data.type=="success")
                        $('#sondage-form')[0].reset();
                    }
                }

            });
            return false;
        }
    })
    
      
      });
        
   </script> 
 
</body>
</html>