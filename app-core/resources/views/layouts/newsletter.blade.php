@php
setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
$date= (strftime("%A %d %B %Y")); 
@endphp
<html>
   <head>
      <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
      <meta charset="UTF-8" />
      <title>linformation.ma</title>
      <style media="only screen and (max-width: 599px)" type="text/css">
         @media only screen and (max-width:599px) {
         table.w320 {
         width: 320px !important;
         }
         .text-nav2 {
         display: none!important;
         }
         .text-nav a {
         font-size: 8px;
         line-height: 12px;
         }
         }
      </style>
      <meta content="width=device-width, initial-scale=1.0" name="viewport" />
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162067724-1" type="3a692fc8f5095639b4c2186b-text/javascript"></script>
      <script type="3a692fc8f5095639b4c2186b-text/javascript">
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         
         gtag('config', 'UA-162067724-1');
      </script>
   </head>
   <body style="margin: 0px;">
      <!-- Start Header -->
  
      <table width="100%" style="max-width: 680px;" align="center" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td align="right"> <table border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="35" valign="top"> </td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td> <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000"> <tbody> <tr> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="5" valign="top"></td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td> <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> <tbody> <tr> <td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td> </tr> </tbody> </table> <div class="text-button-red" style="color:#ffffff; font-family:Arial,sans-serif; font-size:12px; line-height:16px; text-align:center; text-transform:uppercase"> <span class="link-white" style="color:#ffffff; text-decoration:none">{{$date}}</span> </div> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%" bgcolor="#000000"> <tbody> <tr> <td height="6" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td> </tr> </tbody> </table> </td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="5" valign="top"> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>
      <table style="max-width: 680px;" width="100%" align="center" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000"> <tbody> <tr> <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="5" valign="top"> </td> <td valign="top"> <div style="font-size:0pt; line-height:0pt; height:4px; background:#000; "><img src="https://linformation.ma/assets/img/spacer.gif" width="1" height="4" style="height:4px" alt=""></div> </td> </tr> </tbody> </table> <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ac0000"> <tbody> <tr> <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td> <td> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> <tbody> <tr> <td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td> </tr> </tbody> </table> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0" width="202" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> <tbody> <tr> <td height="5" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td> </tr> </tbody> </table> <table width="100%" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td class="img" style="font-size:0pt; line-height:0pt; text-align:center"> <div class="img-m-center" style="font-size:0pt; line-height:0pt; text-align:center"> <a href="https://linformation.ma" target="_blank"><img src="https://linformation.ma/assets/img//logo-linformation.png" border="0" width="187" alt=""></a> </div> <div style="font-size:0pt; line-height:0pt;" class="mobile-br-20"></div> </td> </tr> </tbody> </table> </th>  </tr> </tbody> </table> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"> <tbody> <tr> <td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td> </tr> </tbody> </table> </td> <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td> </tr> </tbody> </table> </td> </tr> </tbody> </table>

      <!-- End header -->



      @yield('content')


      <!-- Start footer -->
   <table style="max-width: 680px;" width="100%" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#363731">
   <tbody>
      <tr>
         <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f0f0">
               <tbody>
                  <tr>
                     <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                     <td>
                        <div class="text-footer-c" style="color:#a0a09a; font-family:Arial,sans-serif; font-size:12px; line-height:16px; text-align:center">
                           <p style="padding-top: 20px;" align="center">
                              <img src="https://upload.wikimedia.org/wikipedia/commons/3/33/CNDP_Logo_Quadrie-Ar-Tfg-Fr-24_-_copie.png" width="20%" alt="CNDP"><br><br>
                              <strong style="color:#000">Accréditation N° : D-NL-189/2020</strong>
                           </p>
                        </div>
                     </td>
                     <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                  </tr>
                  <tr>
   <td colspan="5" height="15">
      <table align="center">
         <tbody>
            <tr>
               <td>
                  <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                     <tbody>
                        <tr style="vertical-align: top;" valign="top">
                           <td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                              <table align="center" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" valign="top">
                                 <tbody>
                                    <tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
                                       <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://www.facebook.com/linformation.ma/" target="_blank"><img alt="Facebook" height="32" src="{{url('/')}}/assets/img/facebook2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="Facebook" width="32"></a></td>
                                       <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://www.linkedin.com/company/linformation-ma/?viewAsMember=true" target="_blank"><img alt="LinkedIn" height="32" src="{{url('/')}}/assets/img/linkedin2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="LinkedIn" width="32"></a></td>
                                       <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://linformation.ma/inscription-a-la-newsletter" target="_blank"><img alt="Inscription" height="32" src="{{url('/')}}/assets/img/mail2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="Inscription à la newsletter" width="32"></a></td>
                                       <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://linformation.ma/" target="_blank"><img alt="Web Site" height="32" src="{{url('/')}}/assets/img/website2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="Web Site" width="32"></a></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
   </td>
</tr>
               </tbody>
            </table>
         </td>
      </tr>

   </tbody>
   <tbody><tr>
<td>
<table width="100%" style="max-width: 680px;" align="center" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td align="center"> <br><div style="font-family:arial,sans-serif;font-size:12px;line-height:18px;color:#949490;text-align:center">Pour vous assurer de recevoir nos e-mails, il vous suffit d’ajouter notre adresse<br> e-mail  <strong style="color:#FFF;"><!--email_off-->info@e.information-new.ma<!--/email_off--></strong> à votre liste d’expéditeurs autorisés<br><br></div></td> </tr> </tbody> </table>
</td>
</tr>


</tbody>
</table>

      <!-- End footer -->
       <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3a692fc8f5095639b4c2186b-|49" defer=""></script>
   </body>