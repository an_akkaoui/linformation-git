@section('title', "Contactez-nous")
@section('description', "contactez nous pour tous vos demandes et vos questions")


      @component('components.home.head')
      @endcomponent
<body>
            @component('components.home.header', ['categories' => $categories])
            @endcomponent   

   <!-- Start content -->
   <section class="head ">
      <div class="container py-5">
         <div class="bg-white">
            <h1 class="font-weight-light text-center py-2">Contactez-nous</h1>
           
               <div class="row cats">
                  <div class="col-lg-6 col-md-6 col-sm-12 ">
                     <div class="row p-5">
                        <div class="col-lg-12 my-2">
                           <div class="row">
                              <div class="col-lg-1">
                                 <span class="icons"><i class="far fa-map"></i></span>
                              </div>
                              <div class="col-lg-10">
                                 <h3 class="titres">Adresse</h3>
                                 <p>1, Angle 
                                    rue Socrate et Abdou Taour <br/>
                                    1 eme étage. 
                                    Maarif Ext
                                 </p>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 my-2">
                           <div class="row">
                              <div class="col-lg-1">
                                 <span class="icons"><i class="far fa-envelope"></i></span>
                              </div>
                              <div class="col-lg-10">
                                 <h3 class="titres">Email</h3>
                                 <p >info@wibday.com</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 my-2">
                           <div class="row">
                              <div class="col-lg-1">
                                 <span class="icons"><i class="fas fa-phone-volume"></i></span>
                              </div>
                              <div class="col-lg-10">
                                 <h3 class="titres">Téléphone</h3>
                                 <p style="color:#000;">+212 684 101 010</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 p-5">
                     <form role="form" id="contact-form" method="post" action="contact.php" novalidate="true">
                        <div class="response"></div>
                     <div class="row">
                        
                        <div class="col-lg-6 col-md-12 col-sm-12">
                           <label for="nom">Nom <span class="wpforms-required-label">*</span></label>
                           <input class="form-control" id="nom" name="nom" type="text" placeholder="" required>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                           <label for="prenom">Prénom <span class="wpforms-required-label">*</span></label>
                           <input class="form-control" id="prenom" name="prenom" type="text" placeholder="" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <label for="telephone">Téléphone <span class="wpforms-required-label">*</span></label>
                           <input class="form-control" id="telephone" name="telephone" type="tel" placeholder="" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <label for="email">Email <span class="wpforms-required-label">*</span></label>
                           <input class="form-control" id="email" name="email" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="" required>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <label for="subject">Votre Demande <span class="wpforms-required-label">*</span></label>
                           <select class="form-control text" id="subject" name="subject" required>
                              <option>Demande de devis</option>
                              <option>Rect. des données personnelles</option>
                              <option>Désabonnement</option>
                              <option>Autre</option>
                           </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                           <label for="message">Message <span class="wpforms-required-label">*</span></label>
                           <textarea rows="4" class="form-control" id="message" aria-label="With textarea" placeholder="" name="message" required></textarea>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 py-2" >
                           <button type="submit" class="btn btn-primary"> Envoyer</button>
                        </div>
                     </div>
                  </form>
                  </div>
               </div>
            
         </div>
      </div>
   </section>
   <!-- End content -->
   
   <!-- Modal -->
   
    <div class="modal" class="modal" id="sucess_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
       <div class="modal-dialog ">
           <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Succès</h4>
                   <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
               </div>
              <div class="modal-body">
                <div class="alert alert-success" role="alert">
                      Votre Message a été envoyé avec succès!
                </div>
              </div>
           </div>
      </div>
    </div>
   
   <!-- End Modal -->
   <!-- Start Section Footer -->
      @component('components.home.footer')
      @endcomponent
   <!-- End Section Footer -->
   
   <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
    @component('components.js.form',['url'=>'contact.sendmail'])
    @endcomponent
    <!-- js jquery bootstrap -->
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


  
   <!-- End section slider  -->
   <!-- Start cdn jquery -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <!-- End cdn jquery -->

</body>
</html>