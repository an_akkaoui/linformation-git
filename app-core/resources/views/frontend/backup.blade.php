<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Système de sauvegarde</title>
    
    
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://linformation.ma/assets/tirage_au_sort/assets/css/normalize.css">
    <link rel="stylesheet" href="https://linformation.ma/assets/tirage_au_sort/assets/css/style.css">
    
 

</head>

<body>

    <div class="app">
        <main>
            <header>
                <div class="logo">
                    <a href="">
                        <img src="https://linformation.ma/assets/tirage_au_sort/assets/images/logo-linformation_2020.png" alt="Linformation.ma">
                    </a>
                </div>
            </header>

            <section class="layout ly_upload text-center">

                <!-- error msg  -->

                <div class="alert alert-danger msg_error" role="alert">

                </div>

                <!-- error msg  -->
                


                <h3 class="big_title mb-5">Système de sauvegarde</h3>
                @if(isset($tables) && !empty($tables))
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Nom</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tables as $table)
                        @foreach ($table as $key => $value)
                            <tr>
                              <td><span>{{ $value }}</span><br></td>
                              <td><a href="#" class="recover" data-tbl="{{ $value }}" style="text-decoration: none;color: green;">Récupérer</a>
                            </td>
                            </tr>
                            
                        @endforeach
                    @endforeach
                    
                  </tbody>
                </table>
                @endif
                <button class="btn trier" id="save">Sauvegarder</button>
                
                <button class="btn trier" disabled id="saving" class="d-none">
                  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  Sauvegarde en cours...
                </button>
                    
                <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
                  <div id="liveToast" class="toast align-items-center text-white border-0" role="alert" aria-live="assertive" aria-atomic="true">
                      <div class="d-flex">
                        <div class="toast-body">
                          
                        </div>
                        <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                      </div>
                    </div>
                </div>

            </section>

            <!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->


            <footer>
                <p>Copyright ©Linformation.ma | Tous Droits Résérvés</p>
            </footer>
        </main>


    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
    <script type="text/javascript" data-cfasync="false" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" data-cfasync="false">
         $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
        $("#saving").addClass('d-none');

        var liveToast = document.getElementById('liveToast')

        

        backup_url = "{{ route('voyager.backup_posts') }}"

        
        
        $("#save").click(function(){
            $("#save").addClass('d-none');
            
            $("#saving").removeClass('d-none');
            
            $.get(backup_url, function(data, status){
                
                var toast = new bootstrap.Toast(liveToast);
                $('.toast-body').html('Le système effectue une sauvegarde des articles, veuillez patienter quelques minutes (Cela peut affecter le serveur)...');
                    $("#liveToast").addClass("bg-success");
                    $("#liveToast").removeClass("bg-danger");
                
                if(data == '0'){
                    $('.toast-body').html('Erreur! Vous avez atteint le nombre limité de sauvegardes pour cette annee (10).');
                    $("#liveToast").addClass("bg-danger");
                    $("#liveToast").removeClass("bg-success");
                    
                    $("#save").removeClass('d-none');
            
                    $("#saving").addClass('d-none');

                }else if(data == '1'){
                    
                    $('.toast-body').html('Success!');
                    $("#liveToast").addClass("bg-success");
                    $("#liveToast").removeClass("bg-danger");

                    $("#save").removeClass('d-none');
            
                    $("#saving").addClass('d-none');


                    location.reload(true);
                }else if(data == '2'){
                     $('.toast-body').html('Pas de données à sauvegarder!');
                    $("#liveToast").addClass("bg-danger");
                    $("#liveToast").removeClass("bg-success");
                    
                    $("#save").removeClass('d-none');
            
                    $("#saving").addClass('d-none');
                    location.reload(true);
                }
                
                toast.show();
                
             });
            
        }); 



        $(".recover").click(function(){
            var tbl_name = $(this).data('tbl')
            recover_url = "{{ route('voyager.recover_posts') }}"
            var toast = new bootstrap.Toast(liveToast);
            $('.toast-body').html('Récupération des articles en cours...');
            $("#liveToast").addClass("bg-warning");
            $("#liveToast").removeClass("bg-success");
            toast.show();
            $.post(recover_url, { table_name: tbl_name} , function(data){
                
                
                
                if(data == '1'){
                    $('.toast-body').html('Success! les articles actuels ont été récupérés!');
                    $("#liveToast").removeClass("bg-warning");
                    $("#liveToast").addClass("bg-success");
                    setTimeout(function () {
                        location.reload(true);
                    }, 5000);
                    

                }
                
                toast.show();
                
             });
            
        });    
    </script>

</body>

</html>
