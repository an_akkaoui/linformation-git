@section('title', "Team")
@section('description', "Description dir hna")


      @component('components.home.head')
      @endcomponent
<body>
            @component('components.home.header', ['categories' => $categories])
            @endcomponent   

   <!-- Start content -->
        <div class="container py-5 mt-5"style="background: #fff !important;">
            <div class="row">
                <div class="col-lg-8 mx-auto col-md-12 col-sm-12">
                                  <div class="contents-eqi bg-white">
                <h2 style="color: #ac0000; font-size: 28px; padding: 12px 0px; z-index: 1; position: relative; width: auto; display: inline-block; margin: 0; line-height: 1.5; font-weight: 600;">L'équipe de Linformation.ma</h2>
                <h3 style="font-size: 18px;margin: 12px 0px;">Le Journal Linformation.ma a été fondé en Avril 2022</h3>
                
                <div style="margin: 10px;padding: 20px 0px;">
                    <div class="headinfd" style="margin-bottom: 12px;">
                <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Président :</p>
                <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Ismael Belkhayat Zougari</p>
                </div>
                  <div class="headinfd" style="margin-bottom: 12px;">
                 <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Manger Général :</p>
                  <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Aboussaoud Lamyae</p>
                </div>
                <div class="headinfd" style="margin-bottom: 12px;">
                    <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Directeur de Publication :</p>
                 <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Bouchaib Najjar</p>
                </div>
                
                  <div class="headinfd" style="margin-bottom: 12px;">
                    <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Directeur Financier :</p>
                    <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Fatima Zahra Doghmi</p>
                </div>
                  <div class="headinfd" style="margin-bottom: 12px;">
                    <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Secrétaire de rédaction :</p>
                    <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Wiam Alej</p>
                    </div>
                  <div class="headinfd" style="margin-bottom: 12px;">
                    <p style="margin-bottom: 0.5rem; line-height: 1.2; color: #ac0000; font-size: 1.1rem;">Responsable Technique :</p>
                    <p style="font-size:14px;padding-left: 10px;padding-bottom: 10px;margin: 0;">Ayoub Battah</p>
                 </div>
                </div>

            </div>
                </div>
            </div>
        </div>
   <!-- End content -->

   <!-- Start Section Footer -->
      @component('components.home.footer')
      @endcomponent
   <!-- End Section Footer -->
   
   <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
    @component('components.js.form',['url'=>'contact.sendmail'])
    @endcomponent
    <!-- js jquery bootstrap -->
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


  
   <!-- End section slider  -->
   <!-- Start cdn jquery -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <!-- End cdn jquery -->

</body>
</html>