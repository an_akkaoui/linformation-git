@extends('layouts.master')
@section('title', $categorie['name'])
@section('content')


<div class="col-lg-8 ">
     
                  <div class="row cat categories bg-white mb-2">
                       <div class="catégoriess">
                           <h2 href="#" class="{{ $categorie['slug'] }}">{{ $categorie['name'] }}</h2>
                           </div>
                     @component('components.categories.categorie_post', [ 'posts' => $posts_ordered ])
                     @endcomponent

                     {{ $posts->onEachSide(1)->links('pagination::linformation') }}


                  </div>
               </div>


@endsection
