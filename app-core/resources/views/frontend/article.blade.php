

@extends('layouts.master')
@section('title', $post->title)
@section('description',$post->meta_description)
@isset($post->image)
	@section('image', Voyager::image( $post->image ))
@endisset
@isset($post->meta_keywords)
	@section('meta_keywords', $post->meta_keywords)
@endisset	
@isset($post->seo_title)
	@section('seo_title', $post->seo_title)
@endisset		
@section('og')

@parent
<meta property="og:type"               content="article" />
@endsection

@section('content')

@component('components.blog.post', ['post' => $post, 'categorie' => $categorie, 'top_posts' => $top_posts, 'related_posts' =>$related_posts, 'zones' => $zones])
@endcomponent

@endsection
