@extends('layouts.master')
@section('title', "Confirmation")
@section('description',"Confirmation d'inscription a la newsletter")

@section('content')
<script type="text/javascript">
fbq('track', 'CompleteRegistration');	
</script>
<div class="container body confirmation_page">
	<div class="row">
		<div class=" article">
				<h1>Votre inscription est bien enregistrée!</h1>	
				<p>un e-mail automatique de confirmation vient de vous-être envoyé !</p>
				<p> Merci pour votre confiance!</p>
		</div>
	</div>
	
   
</div><!-- End container -->
@endsection
