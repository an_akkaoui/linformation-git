<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; CHARSET=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>@if(isset($post)) {{ $post[0]->title }} @endif</title>

  <style type="text/css">
    body {
      font-family: Verdana, Geneva, sans-serif;
      margin: 0;
    }

    img {
      width: 100%;
      border: 0;
      display: block;
    }

    td {
      padding: 0;
    }

    @media screen and (max-width: 400px) {
      .col {
        width: 100% !important;
        display: block;
      }
    }
  </style>
</head>

<body>
  <!-- header  -->
  <table style="
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        max-width: 750px;
        margin-left: auto;
        margin-right: auto;
      ">
    <tr>
      <td>
        <a href="https://linformation.ma/">
          <img src="https://linformation.ma/storage/ban-linformaion.ma/ban-linformation.png" alt="Linformation.ma" />
        </a>
      </td>
    </tr>
    <tr>
      <td align="right" style="font-size: 14px; margin: 0; font-weight: bold">
        <p style="
              display: inline-block;
              padding: 10px;
              color: rgb(0, 0, 0);
              font-size: 12px;
            ">
          {{ $formatted_time }}
        </p>
      </td>
    </tr>
  </table>
  <!-- header  -->

  <table style="border-collapse: collapse;border-spacing: 0;width: 100%;max-width: 750px;margin-left: auto;margin-right: auto;">
    <tr>
      <td>
        <a href="{{ $post[0]->postUrl() }}" style="text-decoration: none;color: black;">
          <h2 style="text-align: center;font-size: 26px;">
            {{ Str::limit($post[0]->title, 120,) }} 
          </h2>
        </a>

      </td>
    </tr>

    <tr>
      <td>
        <table style="border-collapse: collapse; border-spacing: 0; width: 100%">
          <tbody>
            <tr>
              <td style="width: 5%; background-color: #F2F1F0"></td>

              <td style="width: 95%">
                <a href="{{ $post[0]->postUrl() }}">
                  <img src="{{ Voyager::image($post[0]->image) }}" alt="" />
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>

    <tr>
      <td>
        <table style="border-collapse: collapse;border-spacing: 0;width: 100%;max-width: 750px;margin-left: auto;margin-right: auto;">
          <tr>
            <td class="col" style="width: 60%">
              <p style="
                    font-size: 14px;
                    line-height: 1.8;
                    word-break: break-word;
                    text-align: justify;
                    margin: 0;
                    padding: 20px 60px;
                  background-color: #F2F1F0;
                    color: #555;">{{ $post[0]->excerpt }}       </td>

            <td class="col" align="center" style="width: 40%">
              <p>
                <a href="{{ $post[0]->postUrl() }}"
                  style="
                      text-decoration: none;
                      color: #ffffff;
                      width: 130px;
                      display: inline-block;
                      background-color: #AC0000;
                      height: 36px;
                      line-height: 36px;
                      border-radius: 5px;
                      font-size: 14px;
                      letter-spacing: 1px;
                    ">Lire la suite</a>
              </p>

            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td style="padding:0 0 20px">
        <table style="border-collapse: collapse; border-spacing: 0; width: 100%">
          <tbody>
            <tr>
              <td style="width: 5%; background-color: #F2F1F0"></td>

              <td style="width: 95%">
                {!! $banner_alert['extracted_script_bannier'][17] !!}
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>

  </table>

  <!-- footer -->
  <table style="
        background-color: #f1f1f1;
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        max-width: 750px;
        margin-left: auto;
        margin-right: auto;
      ">
    <tr>
      <td align="center">
        <img src="https://upload.wikimedia.org/wikipedia/commons/3/33/CNDP_Logo_Quadrie-Ar-Tfg-Fr-24_-_copie.png"
          alt="CNDP" style="max-width: 160px" />
      </td>
    </tr>
    <tr>
      <td align="center">
        <p>Accréditation N° : D-NL-189/2020</p>
      </td>
    </tr>
    <tr>
      <td align="center">
        <table>
          <tr align="center" style="
                vertical-align: top;
                display: inline-block;
                text-align: center;
              " valign="top">
            <td style="
                  word-break: break-word;
                  vertical-align: top;
                  padding-bottom: 0;
                  padding-right: 2.5px;
                  padding-left: 2.5px;
                " valign="top">
              <a href="http://e.information-new.ma/tk/t/2/225651363d7f/1341657ab/3523418/51212146baa/"
                target="_blank"><img alt="Facebook" height="32" src="https://linformation.ma/assets/img/facebook2x.png"
                  style="
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                      height: auto;
                      border: 0;
                      display: block;
                      max-width: 32px;
                    " title="Facebook" width="32" /></a>
            </td>
            <td style="
                  word-break: break-word;
                  vertical-align: top;
                  padding-bottom: 0;
                  padding-right: 2.5px;
                  padding-left: 2.5px;
                " valign="top">
              <a href="http://e.information-new.ma/tk/t/2/225651363d7f/234169953/3523418/51212146baa/"
                target="_blank"><img alt="LinkedIn" height="32" src="https://linformation.ma/assets/img/linkedin2x.png"
                  style="
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                      height: auto;
                      border: 0;
                      display: block;
                      max-width: 32px;
                    " title="LinkedIn" width="32" /></a>
            </td>
            <td style="
                  word-break: break-word;
                  vertical-align: top;
                  padding-bottom: 0;
                  padding-right: 2.5px;
                  padding-left: 2.5px;
                " valign="top">
              <a href="http://e.information-new.ma/tk/t/2/225651363d7f/3341657c4/3523418/51212146baa/"
                target="_blank"><img alt="Inscription" height="32" src="https://linformation.ma/assets/img/mail2x.png"
                  style="
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                      height: auto;
                      border: 0;
                      display: block;
                      max-width: 32px;
                    " title="Inscription à la newsletter" width="32" /></a>
            </td>
            <td style="
                  word-break: break-word;
                  vertical-align: top;
                  padding-bottom: 0;
                  padding-right: 2.5px;
                  padding-left: 2.5px;
                " valign="top">
              <a href="http://e.information-new.ma/tk/t/2/225651363d7f/43416713d/3523418/51212146baa/"
                target="_blank"><img alt="Web Site" height="32" src="https://linformation.ma/assets/img/website2x.png"
                  style="
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                      height: auto;
                      border: 0;
                      display: block;
                      max-width: 32px;
                    " title="Web Site" width="32" /></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="center">
        <p style="
              color: rgba(241, 241, 241, 0.945);
              padding: 15px;
              background-color: #363731;
              margin: 0;
              font-size: 12px;
              line-height: 1.2;
              word-break: break-word;
              text-align: center;
              margin-top: 0;
              margin-bottom: 0;
            ">
          <span style="display: block">
            Pour vous assurer de recevoir nos e-mails, il vous suffit
            d’ajouter notre adresse</span>
          <span style="display: block">e-mail info@e.information-new.ma à votre liste d’expéditeurs
            autorisés</span>
        </p>
      </td>
    </tr>
  </table>
  <!-- footer -->
</body>

</html>

<!-- source  -->

<!-- <tr>
      <td align="right">
       
      </td>
    </tr> -->

<!-- banner saham  -->

<!-- <tr>
      <td colspan="2">
        <a href="https://linformation.ma/">
          <img src="https://linformation.ma/storage/campaigns/October2021/UXbrMvp4SV9VowBbgvAF.jpg" alt="">
        </a>
      </td>
    </tr> -->