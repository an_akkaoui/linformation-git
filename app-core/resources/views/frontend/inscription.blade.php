@section('title', "Inscription à la newsletter")
@section('crf')
@parent
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@component('components.home.head')
@endcomponent
<body style="background: #f0f2f5 ">
   <!-- Start Section Header -->
   @component('components.home.header', ['categories' => $categories])
   @endcomponent

      <!-- Start content -->
   <div class="container p-4">
      <div class="card-body bg-white p-3">
         <div class="row justify-content-center mb-3 mt-3 ">
            <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
               <picture >
                  <img src="{{asset('assets/img/cantact.png') }}" class="img-fluid img-thumbnail" alt="...">
               </picture>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
               <form role="form" id="inscription-form" method="post" action="inscription.php" novalidate="true">
                  <div class="response"></div>
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="mb-2">
                        <label for="exampleFormControlInput1" class="form-label">Civilité :</label>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input" type="radio" name="civilite" id="inlineRadio1" value="Mme" checked>
                           <label class="form-check-label" for="inlineRadio1">Mme</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input" type="radio" name="civilite" id="inlineRadio2" value="Mr">
                           <label class="form-check-label" for="inlineRadio2">Mr</label>
                        </div>
                        <div class="form-check form-check-inline">
                           <input class="form-check-input" type="radio" name="civilite" id="inlineRadio3" value="Mlle">
                           <label class="form-check-label" for="inlineRadio3">Mlle</label>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Email</label>
                        <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nom</label>
                        <input type="text" name="nom" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Prénom</label>
                        <input type="text" name="prenom" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Téléphone</label>
                        <input type="tel" name="telephone" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Age</label>
                        <select class="form-select fc" name="age" required>
                                             <option selected>-----</option>
                                             <option value="18 - 25">18 - 25</option>
                                             <option value="25 - 35">25 - 35</option>
                                             <option value="35 - 45">35 - 45</option>
                                             <option value="45+">45+</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Ville</label>
                        <input type="text" name="ville" class="form-control" id="exampleFormControlInput1" placeholder="" required>
                     </div>
                  </div>
                  <!-- Start section fonction -->
                  <div class="col-lg-6 col-md-6 col-sm-12">
                     <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Fonction</label>
                        <select class="form-select fc fonctions" name="fonction" required>
                           <option selected>-----</option>
                           <option value="Cadre supérieure">Cadre supérieure</option>
                           <option value="Cadre">Cadre</option>
                           <option value="Direction générale">Direction générale</option>
                           <option value="Professions libérales">Professions libérales</option>
                           <option value="Assistante de direction">Assistante de direction</option>
                           <option value="Etudiant(e)">Etudiant(e)</option>
                           <option value="Retraité">Retraité</option>
                           <option value="Employé (e) de bureau">Employé (e) de bureau</option>
                           <option value="Femme au foyer">Femme au foyer</option>
                           <option value="Autres">Autres</option>
                        </select>
                     </div>
                     
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="mb-3 DepartementC d-none grouphidden">
                        <label for="exampleFormControlInput1" class="form-label" >Departement</label>
                        <select class="form-select fc" name="DepartementCadre">
                           <option selected>-----</option>
                           <option value="Direction Financière">Direction Financière</option>
                           <option value="Direction Juridique">Direction Juridique</option>
                           <option value="Direction de la Communication">Direction de la Communication</option>
                           <option value="Direction Générale">Direction Générale</option>
                           <option value="Direction Ressources Humaines">Direction Ressources Humaines</option>
                           <option value="Direction Informatique">Direction Informatique</option>
                           <option value="Direction logistique">Direction logistique</option>
                           <option value="Direction Marketing">Direction Marketing</option>
                           <option value="Autres">Autres</option>
                        </select>
                     </div>
                     <div class="mb-3 ProfessionL d-none grouphidden">
                        <label for="exampleFormControlInput1" class="form-label" >Departement</label>
                        <select class="form-select fc" name="departementProfessionL">
                           <option selected>-----</option>
                           <option value="Architecte">Architecte</option>
                           <option value="Avocat">Avocat</option>
                           <option value="Administrateur judiciaire">Administrateur judiciaire</option>
                           <option value="Expert-comptable">Expert-comptable</option>
                           <option value="Médecin">Médecin</option>
                           <option value="Notaire">Notaire</option>
                           <option value="Autres">Autres</option>
                        </select>
                     </div>
                     <div class="mb-3 assistanteD d-none grouphidden">
                        <label for="exampleFormControlInput1" class="form-label" >Departement</label>
                        <select class="form-select fc" name="departementassistanteD">
                           <option selected>-----</option>
                           <option value="Assistante / secrétaire">Assistante / secrétaire</option>
                           <option value="Assistante de département">Assistante de département</option>
                           <option value="Assistante de direction">Assistante de direction</option>
                           <option value="Office Manager">Office Manager</option>
                           <option value="Autres">Autres</option>
                        </select>
                     </div>
                     <div class="mb-3 autres d-none grouphidden">
                        <label for="exampleFormControlInput1" class="form-label">Autre</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Autres ..." name="departementAutres">                
                     </div>
                  </div>
                  <!-- End section fonction -->
                  <div class="col-lg-12 col-md-12 col-sm-12">
                     <p style="font-size: 1rem; text-align: justify;word-break: break-all;">
                        Conformément à la loi 09-08, vous disposez d’un droit d’accès, de rectification et 
                        d’opposition au traitement de vos données personnelles. Ce traitement a 
                        été déclaré par la CNDP sous le n° D-NL-189/2020 .
                     </p>
                     <div class="form-check">
                        <input class="form-check-input" name="acceptance1" type="checkbox" value="1" id="flexCheckDefault" required>
                        <label class="form-check-label" for="flexCheckDefault" style="word-break: break-all;
                           font-size: 0.9rem;"  >
                        J’ai lu et j’accepte <a href="https://linformation.ma/assets/pdf/note-legale.pdf" target="_blank" rel="noopener noreferrer" style="color: #ac0000;
                           text-decoration: underline;">la note légale "Linformation.ma"</a> , notamment la mention relative à la protection des données personnelles.
                        </label>
                     </div>
                     <div class="form-check">
                        <input class="form-check-input" name="acceptance2" type="checkbox" value="1" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault" style="word-break: break-all;
                           font-size: 0.9rem;">
                        J'accepte de recevoir la newsletter quotidienne Linformation.ma.
                        </label>
                     </div>
                     <div class="form-check">
                        <input class="form-check-input" name="acceptance3" type="checkbox" value="1" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault" style="word-break: break-all;
                           font-size: 0.9rem;">
                        J'accepte de recevoir la newsletter des partenaires commerciaux de Wib Day.
                        </label>
                     </div>
                  </div>
                  <div class="d-grid gap-2 mt-3 mx-auto ">
                     <button class="btn btn-primary" type="submit">Envoyer</button>
                  </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <!-- End content -->

   @component('components.home.footer')
   @endcomponent
   </div>
   </div>
   <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
     <!-- js jquery bootstrap -->
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


  
   <!-- End section slider  -->
   <!-- Start cdn jquery -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   <!-- End cdn jquery -->

    @component('components.js.form',['url'=>'inscription.ins'])
    @endcomponent
</body>


