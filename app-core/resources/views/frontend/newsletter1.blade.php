@extends('layouts.newsletter')
@section('content')
   @php

      $catcolor['1'] = '#e27704';
      $catcolor['2'] = '#607d8b';
      $catcolor['3'] = '#f44336';
      $catcolor['4'] = '#4caf50';
      $catcolor['5'] = '#8f2b03'; 
      $catcolor['6'] = '#1e73be';
      $catcolor['7'] = '#cb23db';

   @endphp
      <!-- Banner start -->
      <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="max-width: 680px;" width="100%">
               <tbody>
                  <tr>
                     <td align="center">
                        @if(isset($zones['ads_newsletter_lien'][4]) && !empty($zones['ads_newsletter_lien'][4]))
                        <a class="img-ads" href="{{$zones['ads_newsletter_lien'][4]}}" target="_blank">
                           @if(isset($zones['ads_newsletter_image'][4]) && !empty($zones['ads_newsletter_image'][4]))
                              <img style="width:100%!important" src="{{Voyager::image($zones['ads_newsletter_image'][4])}}">
                           @endif
                        </a>
                        @endif
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3"> <img alt="" class="CToWUd" height="20" src="https://linformation.ma/assets/img/spacer.gif" style="display:block" /></td>
                  </tr>
               </tbody>
            </table>
      <!-- Banner end -->

      @foreach($categories as $categorie)

      <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="w320" style=" max-width: 680px;" width="100%">
         <tbody>
            <tr>
               <td style="margin:0 0 5px;font-weight:bold;text-transform:uppercase;font-size:20px;font-family:Tahoma, Geneva"> <a style="color:{{ $catcolor[$categorie->id] }};margin:0 0 5px;font-weight:bold;text-transform:uppercase;font-size:20px;font-family:Tahoma, Geneva;text-decoration:none" href="{{url('/').'/news/'.$categorie->slug}}">{{$categorie->name}}</a> </td>
            </tr>
            <tr bgcolor="{{ $catcolor[$categorie->id] }}" height="3px">
               <td> <img alt="" class="CToWUd" src="https://linformation.ma/assets/img/spacer.gif" style="display:block" /></td>
            </tr>
         </tbody>
      </table>

      <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style=" max-width: 680px;" width="100%">
         <tbody>
            <tr>
               <td colspan="5" height="15" style="font-size:1px"> </td>
            </tr>
            <tr>
               <td colspan="5" style="padding-left:3% !important;padding-right: 3% !important;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                     <tbody>
                        @php $x = 1; @endphp
                        @for($i=0; $i < 2; $i++)
                        <tr>
                           <td align="left" valign="top">
                              <div style="text-align:justify;font-family:Tahoma, Geneva,sans-serif;font-size:15px;line-height:15px;font-weight:bold;color:#000;text-decoration:none" target="_blank"><a href="{{$posts[$categorie->id][$i]->postUrl}}" style="color:#3f3f3f;text-decoration:none !important;">{{Str::words($posts[$categorie->id][$i]->title,$words = 50, $end='...')}}</a></div>
                           </td>
                        </tr>
                        <tr>
                           <td align="left" style="padding-top:20px;" valign="top">
                              <a href="{{$posts[$categorie->id][$i]->postUrl}}"><img src="{{$posts[$categorie->id][$i]->thumb_150x150}}" style="float: left; min-height: 90px; margin-bottom: 10px; margin-right: 10px; max-height: 100px;;" /></a>
                              <div style="font-family:arial,sans-serif;font-size:14px;line-height:18px;color:#949490;text-align:justify">{{Str::words($posts[$categorie->id][$i]->excerpt,$words = 50, $end='...')}} <span class="text-price"><a href="{{$posts[$categorie->id][$i]->postUrl}}" style="color:#4a4a44; font-family:Arial,sans-serif; font-size:14px; line-height:24px; text-align:left; text-decoration:none; ">[suite]</a> - <span style="color:#4a4a44; font-family:Arial,sans-serif; font-size:14px; line-height:24px; text-align:left; text-decoration:none; "><a href="#" style="text-decoration:none; color:#000">{{$posts[$categorie->id][$i]->trimSourceLink}}</a></span> </span></div>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="5" height="15"> </td></tr>
                            <tr>
                              @if($x != 2)
                                 <td colspan="5" height="15" style="border-top:1px solid #e6e7e8;font-size:1px;"> </td>
                              @else
                                 <tr> <td style="line-height:1px;font-size:1px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;margin-top:0px;margin-bottom:0px;margin-right:0px;margin-left:0px;height:10px;"> </td> </tr>
                                 <tr> <td align="right" style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;line-height:12px;height:10px;" valign="top"> <a href="{{$posts[$categorie->id][$i]->categoryUrl}}" style="text-decoration:none;" target="_blank"><font color="{{$catcolor[$categorie->id]}}"><b>»LIRE LA SUITE</b></font></a></td> </tr>
                              @endif
                           </tr>

                           <tr> <td style="line-height:1px;font-size:1px;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;margin-top:0px;margin-bottom:0px;margin-right:0px;margin-left:0px;height:10px;"> </td> </tr>
                           @php $x++; @endphp
                        @endfor
                     </tbody>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      @if($categorie->id == 7)
      @break;
      @endif
      <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="max-width: 680px;" width="100%"> <tbody> <tr> <td align="center" style="padding: 0px 5.883%;">
            @if(isset($zones['ads_newsletter_lien'][$categorie->pub_id]) && !empty($zones['ads_newsletter_lien'][$categorie->pub_id]))
               <a class="img-ads" href="{{ $zones['ads_newsletter_lien'][$categorie->pub_id] }}" target="_blank">
            
            @if(isset($zones['ads_newsletter_image'][$categorie->pub_id]) && !empty($zones['ads_newsletter_image'][$categorie->pub_id]))
               <img style="width:100%!important;" src="{{ Voyager::image($zones['ads_newsletter_image'][$categorie->pub_id]) }}">
            @endif
            </a>
         @endif
      </td> </tr> <tr> <td colspan="3"> <img alt="" class="CToWUd" height="20" src="https://linformation.ma/assets/img/spacer.gif" style="display:block"></td> </tr> </tbody> </table>
      @endforeach

@endsection