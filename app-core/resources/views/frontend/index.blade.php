@extends('layouts.master')
@section('title', 'Le leader de l\'information')
@section('description', 'Suivez l\'actualité nationale et internationale tous les matins avec L\'infomation.ma')
@section('image', url('/')."/assets/img/logo-linformation_2x.png")
@section('content')

<div class="col-md-8 col-sm-12 mt-2">
               <!-- Start section actualité -->
               <div class="sectionb sectionall zwrap">
                  @component('components.categories.actualite', [ 'posts' => $posts[1] ])
                  @endcomponent
               </div>
               <!-- End section actualité -->
               <!-- Start Actualité section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][5]) && !empty($zones["extracted_script_bannier"][5]))
                     {!! $zones['extracted_script_bannier'][5] !!}
                     @endif
                  </div>
               </div>
               <!-- End Actualité section pub-one -->
               <!-- Start section Politique -->
               <div class="sectionb sectionall zwrap">
                  @component('components.categories.politique', [ 'posts' => $posts[2] ])
                  @endcomponent
               </div>
               <!-- End section Politique -->
               <!-- Start Politique section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][6]) && !empty($zones["extracted_script_bannier"][6]))
                     {!! $zones['extracted_script_bannier'][6] !!}
                     @endif
                  </div>
               </div>
               <!-- End Politique section pub-one -->
               <!-- Start section Finance & Economie -->
               <div class="sectionb sectionall zwrap">
                  @component('components.categories.finance-economie', [ 'posts' => $posts[3] ])
                  @endcomponent
               </div>
               <!-- End section Finance & Economie -->
               <!-- Start Finance & Economie section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][7]) && !empty($zones["extracted_script_bannier"][7]))
                     {!! $zones['extracted_script_bannier'][7] !!}
                     @endif
                  </div>
               </div>
               <!-- End Finance & Economie section pub-one -->

               <!-- Start section Sport  -->
               <div class="sectionalplr zwrap">
                   @component('components.categories.sport', [ 'posts' => $posts[4] ])
                  @endcomponent
                  
               </div>
               <!-- End section Sport  -->
               <!-- Start Sport section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][8]) && !empty($zones["extracted_script_bannier"][8]))
                     {!! $zones['extracted_script_bannier'][8] !!}
                     @endif
                  </div>
               </div>
               <!-- End Sport section pub-one -->

               <!-- Start section Culture  -->
               <div class="sectionalplr zwrap">
                 @component('components.categories.culture', [ 'posts' => $posts[5] ])
                  @endcomponent
               </div>
               <!-- End section Culture  -->
               <!-- Start Sport section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][9]) && !empty($zones["extracted_script_bannier"][9]))
                     {!! $zones['extracted_script_bannier'][9] !!}
                     @endif
                  </div>
               </div>
               <!-- End Sport section pub-one -->
               <!-- Start section Monde -->
               <div class="sectionv sectionall zwrap">
                  @component('components.categories.monde', [ 'posts' => $posts[6] ])
                  @endcomponent
               </div>
               <!-- End section Monde -->
               <!-- Start Monde section pub-one -->
               <div class="row p-3">
                  <div class="col">
                     @if(isset($zones["extracted_script_bannier"][10]) && !empty($zones["extracted_script_bannier"][10]))
                     {!! $zones['extracted_script_bannier'][10] !!}
                     @endif
                  </div>
               </div>
               <!-- End Monde section pub-one -->
               <!-- Start section High tech  -->
               <div class="sectionalplr zwrap">
                  @component('components.categories.high-tech', [ 'posts' => $posts[7] ])
                  @endcomponent
               </div>
               <!-- End section High tech  -->
               <!-- Start section video  -->
               <div class="sectionf sectionall nload pt-3">
                  @component('components.categories.video', [ 'posts' => $posts[10] ])
                  @endcomponent
               </div>
               <!-- End section video -->
               <!-- Start section Liftyle & People -->
               <div class="row">
                  
                  <div>
                     <div class="row  mt-2">
                        <!-- Start section Lifestyle -->
                           @component('components.categories.lifestyle', [ 'posts' => $posts[9] ])
                           @endcomponent
                        <!-- End section Lifestyle -->


                        <!-- Start section People -->
                           @component('components.categories.people', [ 'posts' => $posts[11] ])
                           @endcomponent
                        <!-- End section People -->
                     </div>
                  </div>
                  
               </div>
            </div>
           
@endsection