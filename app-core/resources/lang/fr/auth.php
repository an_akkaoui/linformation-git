<?php
return [
    "failed" => "Ces informations ne correspondent pas à nos dossiers.",
    "throttle" => "Trop de tentatives de connexion. S’il vous plaît essayer à nouveau :seconds secondes."
];
