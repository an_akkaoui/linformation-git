<?php

//----------------------------------------------------------------------------
// AdAdmin DATABASE install file
// This software IT'S NOT FREE, you can buy it on CodeCanyon.
// Author page: http://codecanyon.net/user/ginoplusio
//----------------------------------------------------------------------------

include("pons-settings.php");
include("src/_include/comode.php");

?>
<html>
<head>
<title>Installing AdAdmin</title>
<style>
body {background-color:#efefef;color:#111; font-family:arial, sans-serif;font-size:14px;line-height:20px}
a { color:#ff00cc;}
</style>
</head>
<body>
Welcome to AdAdmin installation script.<br>
hello :)<br>
<br>
<?php
if(!Connessione()) die("DB ERROR, please open pons-settings.php and config that file.<br>");

echo "&gt; db connection ok.<br>";

$a[] ="SET sql_mode = '';";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner` (
  `id_banner` int(11) unsigned NOT NULL auto_increment,
  `de_url` varchar(255) NOT NULL default '',
  `dt_giorno1` date NOT NULL default '0000-00-00',
  `dt_giorno2` date NOT NULL default '0000-00-00',
  `nu_pageviews` int(11) unsigned NOT NULL default '0',
  `fl_stato` char(1) NOT NULL default '',
  `nu_clicks` int(11) unsigned NOT NULL default '0',
  `de_nome` varchar(100) NOT NULL default '',
  `de_codicescript` text NOT NULL,
  `de_target` enum('_blank','_self') NOT NULL default '_blank',
  `nu_maxtot` int(11) NOT NULL default '0' COMMENT 'massime impression totali',
  `nu_maxday` int(11) NOT NULL default '0' COMMENT 'massime impression al giorno',
  `dt_maxday_date` date NOT NULL default '0000-00-00',
  `nu_maxday_count` int(11) NOT NULL default '0',
  `cd_campagna` int(11) NOT NULL,
  `cd_posizione` int(11) NOT NULL,
  `nu_width` int(11) NOT NULL,
  `nu_height` int(11) NOT NULL,
  `nu_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY  (`id_banner`),
  KEY `dt_giorno2` (`dt_giorno2`),
  KEY `stato_giorno1_formato` (`fl_stato`,`dt_giorno1`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner_campagne` (
  `id_campagna` int(11) NOT NULL AUTO_INCREMENT,
  `de_titolo` varchar(100) NOT NULL,
  `cd_cliente` int(11) NOT NULL,
  `fl_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_campagna`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;";

$a[] = "INSERT INTO `7banner_campagne` (`id_campagna`, `de_titolo`, `cd_cliente`, `fl_status`) VALUES
(9, 'Mycampaign', 7, 1);";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner_clienti` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `de_nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;";

$a[] = "INSERT INTO `7banner_clienti` (`id_cliente`, `de_nome`) VALUES
(7, 'Myself');";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner_clienti_tbc` (
  `cd_utente` int(11) NOT NULL,
  `cd_cliente` int(11) NOT NULL,
  PRIMARY KEY (`cd_utente`,`cd_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Collega utenti a clienti';";

$a[] = "INSERT INTO `7banner_clienti_tbc` (`cd_utente`, `cd_cliente`) VALUES
(34, 7);";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner_posizioni` (
  `id_posizione` int(11) NOT NULL auto_increment,
  `de_posizione` varchar(20) NOT NULL,
  `nu_width` int(11) NOT NULL,
  `nu_height` int(11) NOT NULL,
  PRIMARY KEY  (`id_posizione`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;";

$a[] = "INSERT INTO `7banner_posizioni` (`id_posizione`, `de_posizione`,`nu_width`,`nu_height`) VALUES
(9, '300x250',300,250);";

$a[] = "CREATE TABLE IF NOT EXISTS `7banner_stats` (
  `id_day` date NOT NULL,
  `nu_pageviews` int(10) unsigned NOT NULL,
  `nu_click` int(10) unsigned NOT NULL,
  `cd_banner` int(11) NOT NULL,
  PRIMARY KEY (`id_day`,`cd_banner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_componenti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descrizione` varchar(255) DEFAULT NULL,
  `urlcomponente` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `urliconamenu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='dati dei componenti' AUTO_INCREMENT=190 ;";

$a[] = "INSERT INTO `frw_componenti` (`id`, `nome`, `descrizione`, `urlcomponente`, `label`, `urliconamenu`) VALUES
(1, 'DEBUGGER', 'Debug tool.', 'componenti/debugger/test.php', 'debugger', 'componenti/debugger/images/debugger_ico.gif'),
(5, 'GESTIONEUTENTI', 'User manager', 'componenti/gestioneutenti/index.php', 'Users', 'componenti/gestioneutenti/images/gestioneutenti_ico.gif'),
(12, 'FRWCOMPONENTI', 'per gestire l''installazione/rimozione di funzionalita e componenti', 'componenti/frwcomponenti/index.php', 'Componenti', 'componenti/frwcomponenti/images/ico.gif'),
(14, 'FRWMODULI', 'Per la creazione di nuovi moduli', 'componenti/frwmoduli/index.php', 'Moduli', 'componenti/frwmoduli/images/ico.gif'),
(15, 'FRWPROFILI', 'Gestione dei profili degli utenti del sistema', 'componenti/frwprofili/index.php', 'Profili utenti', 'componenti/frwprofili/images/ico.gif'),
(58, 'MIOPROFILO', 'Gestione cambio password e altri miei dati', 'componenti/gestioneutenti/mioprofilo.php', 'Edit my profile', 'componenti/gestioneutenti/images/gestioneutenti_ico.gif'),
(61, 'FRWVARS', 'Settaggi', 'componenti/frwvars/index.php', 'Settings', 'icone/cog.png'),
(72, 'BANNER', 'Banner', 'componenti/7banner/index.php', 'Banner', 'icone/shape_square.png'),
(175, '7SETTINGS', 'Gestione impostazioni', 'componenti/7settings/index.php', 'Impostazioni', 'icone/cog.png'),
(187, 'CAMPAGNE', 'Campaigns manager', 'componenti/7campagne', 'Campaigns', NULL),
(188, 'CLIENTI', 'Clients manager', 'componenti/7clienti/index.php', 'Clients', NULL),
(189, 'POSIZIONI', 'Positions manager', 'componenti/7posizioni/index.php', 'Positions', NULL),
(194, 'CONSTANTSSETTINGS', 'Vars and settings', 'componenti/frwconstants/index.php', 'Settings', NULL);";



$a[] = "CREATE TABLE IF NOT EXISTS `frw_com_mod` (
  `idcomponente` int(11) NOT NULL DEFAULT '0',
  `idmodulo` int(11) NOT NULL DEFAULT '0',
  `posizione` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcomponente`,`idmodulo`,`posizione`),
  KEY `idcomponente` (`idcomponente`,`idmodulo`),
  KEY `posizione` (`posizione`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

";

$a[] = "INSERT INTO `frw_com_mod` (`idcomponente`, `idmodulo`, `posizione`) VALUES
(14, 1, 0),
(58, 1, 0),
(61, 1, 0),
(12, 1, 1),
(72, 18, 1),
(188, 18, 2),
(187, 18, 3),
(189, 18, 4),
(5, 1, 6),
(15, 1, 7),
(194,1,0);";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_extrauserdata` (
  `cd_user` int(10) unsigned NOT NULL DEFAULT '0',
  `de_email` varchar(200) NOT NULL DEFAULT '',
  `dt_datacreazione` date NOT NULL DEFAULT '0000-00-00',
  `nu_costo` smallint(5) NOT NULL DEFAULT '0',
  `de_temp` varchar(200) NOT NULL DEFAULT '',

  PRIMARY KEY (`cd_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

$a[] = "INSERT INTO `frw_extrauserdata` (`cd_user`, `de_email`, `dt_datacreazione`, `nu_costo`, `de_temp`) VALUES
(36, '', '2015-08-29', 0,'');";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_funzionalita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcomponente` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(100) NOT NULL DEFAULT '',
  `descrizione` varchar(255) DEFAULT NULL,
  `label` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idcomponente` (`idcomponente`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='funzionalita dei componenti' AUTO_INCREMENT=214 ;";

$a[] = "INSERT INTO `frw_funzionalita` (`id`, `idcomponente`, `nome`, `descrizione`, `label`) VALUES
(1, 1, 'DEBUGGER', 'Debugger', 'Debugger tool for support'),
(8, 5, 'READ', 'UTENTI possibilita di vedere gli utenti', 'lettura'),
(9, 5, 'WRITE', 'UTENTI possibilita di modificare/aggiungere/togliere utenti', 'scrittura'),
(20, 12, 'FRWCOMPONENTI', 'gestione componenti', 'gestione componenti'),
(24, 14, 'FRWMODULI', 'Per abilitare la possibilita di gestire moduli', 'Gestione moduli'),
(25, 15, 'FRWPROFILI', 'Per abilitare il componente che crea i profili', 'Per abilitare il componente che crea i profili'),
(78, 58, 'MIOPROFILO', 'MIOPROFILO', 'MIOPROFILO'),
(81, 61, 'FRWVARS', 'FRWVARS', 'FRWVARS'),
(94, 72, 'BANNER', 'BANNER', 'BANNER'),
(199, 175, '7SETTINGS', '7SETTINGS', '7SETTINGS'),
(211, 187, 'CAMPAGNE', 'CAMPAGNE', 'CAMPAGNE'),
(212, 188, 'CLIENTI', 'CLIENTI', 'CLIENTI'),
(213, 189, 'POSIZIONI', 'POSIZIONI', 'POSIZIONI'),
(218, 194, 'CONSTANTSSETTINGS', 'CONSTANTSSETTINGS', 'CONSTANTSSETTINGS');";





$a[] = "CREATE TABLE IF NOT EXISTS `frw_moduli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `visibile` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `posizione` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;";

$a[] = "INSERT INTO `frw_moduli` (`id`, `nome`, `label`, `visibile`, `posizione`) VALUES
(1, 'Handle config settings', 'Config', 1, 0),
(18, 'Adv server menu', 'Ad Server', 1, 97);";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_profili` (
  `id_profilo` int(3) unsigned NOT NULL DEFAULT '0',
  `de_label` varchar(20) NOT NULL DEFAULT '0',
  `de_descrizione` varchar(255) DEFAULT NULL,
  `chiedita` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_profilo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='profili degli utenti';";

$a[] = "INSERT INTO `frw_profili` (`id_profilo`, `de_label`, `de_descrizione`, `chiedita`) VALUES
(20, 'administrator', 'administrator', ',20,5,15,'),
(5, 'guest', 'guest', ''),
(999999, 'superadmin', 'super', ',10,20,5,999999,15,16,4,');";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_profili_funzionalita` (
  `cd_profilo` int(10) unsigned NOT NULL DEFAULT '999999',
  `cd_modulo` int(10) unsigned NOT NULL DEFAULT '0',
  `cd_funzionalita` int(10) unsigned NOT NULL DEFAULT '0',
  KEY `cd_profilo` (`cd_profilo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='contiene le funzionalita da attivare per quel tipo di prof';";

$a[] = "INSERT INTO `frw_profili_funzionalita` (`cd_profilo`, `cd_modulo`, `cd_funzionalita`) VALUES
(999999, 1, 8),
(999999, 1, 9),
(20, 1, 1),
(20, 1, 8),
(20, 1, 9),
(999999, 1, 20),
(999999, 1, 24),
(999999, 1, 25),
(20, 1, 78),
(5, 1, 78),
(999999, 1, 78),
(999999, 1, 81),
(5, 18, 94),
(20, 18, 94),
(999999, 18, 212),
(20, 18, 212),
(999999, 18, 211),
(20, 18, 211),
(20, 18, 213),
(999999, 18, 213),
(999999, 18, 94),
(999999, 1, 1),
(20, 1, 1),
(999999, 1, 1),
(999999, 1, 218),
(20, 1, 218);";


$a[] = "CREATE TABLE IF NOT EXISTS `frw_utenti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(60) NOT NULL DEFAULT '',
  `nome` varchar(100) NOT NULL DEFAULT '',
  `cognome` varchar(100) NOT NULL DEFAULT '',
  `fl_attivo` int(1) unsigned NOT NULL DEFAULT '1',
  `cd_profilo` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='utenti dell''ambiente' AUTO_INCREMENT=37 ;";

$a[] = "INSERT INTO `frw_utenti` (`id`, `username`, `password`, `nome`, `cognome`, `fl_attivo`, `cd_profilo`) VALUES
(36, 'admin', 'BTIKMQEwXGIHYw==', 'Gengis', 'Kahn', 1, 20),
(34, 'gu', 'BTQKIA==', 'John', 'Snow', 1, 5);";

$a[] = "CREATE TABLE IF NOT EXISTS `frw_ute_fun` (
  `idutente` int(11) NOT NULL DEFAULT '0',
  `idfunzionalita` int(11) NOT NULL DEFAULT '0',
  `idmodulo` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `UNICO` (`idfunzionalita`,`idmodulo`,`idutente`),
  KEY `idutente` (`idutente`,`idfunzionalita`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='collegamento utenti - funzionalita';";


$a[] = "CREATE TABLE IF NOT EXISTS `frw_vars` (
  `id_var` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `de_nome` varchar(50) CHARACTER SET utf8 NOT NULL,
  `de_value` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_var`),
  UNIQUE KEY `label_unica` (`de_nome`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;";

$a[] = "INSERT INTO `frw_vars` (`id_var`, `de_nome`, `de_value`) VALUES
(1, 'COLLATIONCONNECTIONQUERY', 'SET NAMES ''utf8'';'),
(21, 'CREA_FUNZIONI_AUTOMATICAMENTE', '1'),
(31, 'CONST_MONEY', '$'),
(32, 'CONST_SERVER_EMAIL_ADDRESS', 'noreply@yourserver.com'),
(33, 'CONST_DATEFORMAT', 'mm/dd/yyyy'),
(34, 'CONST_LOGO', 'https://s3.envato.com/files/182543336/thumb2.jpg');";


if (!table_exists("frw_vars")) {
	echo "&gt; create tables and import data.<br>";


	foreach ($a as $s) {

		$conn->query($s) or die("Error executing query: <pre><code>$s</code></pre> Remove all the tables and try again.<br><br>Error:<br><br><b>".$conn->error."</b>");

	}

	echo "&gt; install completed.<br>";

}



if (table_exists("frw_vars")) {
	echo "&gt; install already done.<br>";
	echo "&gt; running updates:<br>";

			
			$q = execute_scalar( "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_SCHEMA='".DEFDBNAME."' AND TABLE_NAME = '7banner' AND COLUMN_NAME = 'nu_maxclick'");
			if($q==0) {
				echo "&gt; adding `nu_maxclick`.<br>";
				$sql = "ALTER TABLE  `7banner` ADD  `nu_maxclick` INT UNSIGNED NOT NULL default '0' COMMENT  'max click per day' AFTER  `nu_maxday` ;";
				$conn->query($sql) or die("Error while upgrading your DB for max click limit. ".$conn->error." sql='$sql'<br>");
			}
			
			
			$q = execute_scalar( "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_SCHEMA='".DEFDBNAME."' AND TABLE_NAME = '7banner' AND COLUMN_NAME = 'nu_price'");
			if($q==0) {
				echo "&gt; adding `nu_price`.<br>";
				$sql = "ALTER TABLE  `7banner` ADD  `nu_price` DECIMAL( 19, 4 ) NOT NULL default '0.0000';";
				$conn->query($sql) or die("Error while upgrading your DB for nu_price value. ".$conn->error." sql='$sql'<br>");
			}
			
	echo "&gt; check and updates records...<br>";

			$conn->query("INSERT IGNORE INTO frw_componenti (id ,nome ,descrizione ,urlcomponente ,label ,urliconamenu) VALUES ( '194','CONSTANTSSETTINGS','Vars and settings','componenti/frwconstants/index.php','Settings','')");
			$conn->query("INSERT IGNORE INTO frw_com_mod (idcomponente ,idmodulo,posizione) VALUES ( '194','1','0')");
			$conn->query("INSERT IGNORE INTO frw_funzionalita (id ,idcomponente ,nome ,descrizione ,label) VALUES ( '218','194','CONSTANTSSETTINGS','CONSTANTSSETTINGS','CONSTANTSSETTINGS')");
			$conn->query("INSERT IGNORE INTO frw_profili_funzionalita (cd_profilo ,cd_modulo ,cd_funzionalita) VALUES ( '999999','1','218')");
			$conn->query("INSERT IGNORE INTO frw_profili_funzionalita (cd_profilo ,cd_modulo ,cd_funzionalita) VALUES ( '20','1','218')");
			$conn->query("INSERT IGNORE INTO frw_vars (id_var,de_nome,de_value) VALUES ( 31,'CONST_MONEY','$')");
			$conn->query("INSERT IGNORE INTO frw_vars (id_var,de_nome,de_value) VALUES ( 32,'CONST_SERVER_EMAIL_ADDRESS','noreply@yourserver.com')");
			$conn->query("INSERT IGNORE INTO frw_vars (id_var,de_nome,de_value) VALUES ( 33,'CONST_DATEFORMAT','mm/dd/yyyy')");
			$conn->query("INSERT IGNORE INTO frw_vars (id_var,de_nome,de_value) VALUES ( 34,'CONST_LOGO','https://s3.envato.com/files/182543336/thumb2.jpg')");

	echo "&gt; done.<br>";
	echo "&gt; Please, check your configuration in Config > Settings at first login.<br>";


}


echo "&gt; be sure that <code>".WEBURL."/data/logs</code> folder is writeable.<br>";
echo "&gt; be sure that <code>".WEBURL."/data/dbimg/7banner</code> folder is writeable.<br>";

echo "&gt; login with user <code>admin</code> and password <code>admin</code> <a href='".WEBURL."'>here</a> and change the default password.<br>";
echo "&gt; remove the install.php script for safe.";
?>
</body>
</html>
