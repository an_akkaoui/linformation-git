<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<link rel="stylesheet" type="text/css" href="##root##src/template/stile.css?##rand##"> <!-- stili comuni -->
	<link rel="stylesheet" type="text/css" href="##root##data/##DOMINIO##/stile.css"/> <!-- stili del tema -->

	##JQUERYINCLUDE##
	<script language="JavaScript" src="##root##src/template/comode.js?##rand##"></script>

	<script>
	jQuery(document).ready(function($) {
		//...qui...
		setTimeout(function(){
		show("alertBox");},2000);
	} );
	</script>
		<style>
		#promo {position:fixed;right:0;bottom:0;display:inline-block;padding:5px 10px}
	</style>

	<title>LOGIN</title>
</head>
<body onload="document.forms[0].##usernamevar##.focus();">

	<form method="post" action="##actionurl##" id='loginform' name='loginform'>
		<table>
			<tr>
				<td class="logo">
				<img src="##LOGO##" id="logo">
				Linformation.ma</td>
			</tr>
			<tr>
				<td>Utilisateur<br/>
				<input name="##usernamevar##" type="text" maxlength="20" class='f'></td>
			</tr>
			<tr>
				<td>Mot de passe<br/>
				<input name="##passwordvar##" type="password" maxlength="20" class='f' onkeypress="submitonenter('loginform',event,this)"></td>
			</tr>
			<tr>
				<td><input type="button" value="Entrez" id='login' onclick='document.forms[0].submit()'></td>
			</tr>
			<tr>
				<td>
				<div class="message">##msg##</div>
				<br><br>
				</td>
		</table>
	</form>

</body>
</html>